//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Application.h"
const wchar_t* AApplication::s_pMainWindowClassName=L"MainWindowClass";
AApplication::AApplication( HINSTANCE hInstance ):
    m_hInstance( hInstance ),
    m_hWnd( nullptr ),
    m_pFPSController( nullptr )
{
    //  
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = DefaultMainWindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = nullptr;
    wcex.hCursor = nullptr;
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
    wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = s_pMainWindowClassName;
    wcex.hIconSm = nullptr;
    if( !RegisterClassEx( &wcex ) )
        throw std::runtime_error("Register window class was failed!");
    //
    m_pFPSController = new CFPSController();
    //
    wchar_t assetsPath[1024];
    DWORD size = GetModuleFileNameW( nullptr , assetsPath , 1024 );
    wchar_t* lastSlash = wcsrchr( assetsPath , L'\\' );
    if( lastSlash )
        *(lastSlash + 1) = L'\0';
    //
    m_cAssetRootPath = assetsPath;
}
AApplication::~AApplication()
{
    CCommonLib::SafeReleaseObject( &m_pFPSController );
    //
    if( m_hWnd != nullptr)
        DestroyWindow( m_hWnd );
    //
    UnregisterClass( s_pMainWindowClassName , m_hInstance );
}
void AApplication::Run()
{
    //
    if( m_hWnd == nullptr)
        throw std::runtime_error("Window handle was empty,please create window handle before Run().");
    //
    OnInit();
    //
    MSG msg = {0};
    m_pFPSController->Reset();
    while( WM_QUIT != msg.message )
    {
        if( PeekMessage( &msg, nullptr, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        //
        OnUpdate();
        //
        m_pFPSController->UpdateAndSleep();
    }
    //
    OnDestroy();
}
LRESULT CALLBACK AApplication::DefaultMainWindowProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    //PAINTSTRUCT ps;
    //HDC hdc;
    AApplication* pApp = reinterpret_cast<AApplication*>( GetWindowLongPtr( hWnd , GWLP_USERDATA ) );
    switch( message )
    {
    case WM_CREATE:
        {
          LPCREATESTRUCT pCreateStruct = reinterpret_cast<LPCREATESTRUCT>( lParam );
          SetWindowLongPtr( hWnd , GWLP_USERDATA, reinterpret_cast<LONG_PTR>( pCreateStruct->lpCreateParams ) );
        } 
        break;
    case WM_PAINT:
        //hdc = BeginPaint( hWnd, &ps );
        //EndPaint( hWnd, &ps );
        break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;
    default:
        break;
    }
    //
    return DefWindowProc(hWnd, message, wParam, lParam);
}
void AApplication::OnInit()
{}
void AApplication::OnUpdate()
{}
void AApplication::OnDestroy()
{}
std::wstring AApplication::AssetFullPath(const wchar_t* pPath)
{
  return m_cAssetRootPath + pPath;
}