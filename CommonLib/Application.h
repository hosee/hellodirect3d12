//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "stdexcept"
//
#include "windows.h"
//
#include "CommonLib.h"
#include "Timer.h"
#include "FPSController.h"
class AApplication
{
protected:
    AApplication( HINSTANCE hInstance );
public:
    virtual ~AApplication();
    void Run();
protected:
    static LRESULT CALLBACK DefaultMainWindowProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );
    //Override
    virtual void OnInit();
    virtual void OnUpdate();
    virtual void OnDestroy();
    //
    std::wstring AssetFullPath(const wchar_t* pPath);
protected:
    static const wchar_t* s_pMainWindowClassName;
protected:
    HINSTANCE m_hInstance;
    HWND m_hWnd;
    std::wstring m_cAssetRootPath;
    CFPSController* m_pFPSController;
};