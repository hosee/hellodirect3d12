//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "CommandAllocator.h"
CCommandAllocator::CCommandAllocator( ID3D12Device* pDevice ,D3D12_COMMAND_LIST_TYPE commandListType ) :
    m_pCommandAllocator(nullptr)
{
    if( FAILED( pDevice->CreateCommandAllocator( commandListType , IID_ID3D12CommandAllocator , (void**)&m_pCommandAllocator ) ) )
        throw std::runtime_error("Create command allocator was failed.");
    //
}
CCommandAllocator::~CCommandAllocator()
{
    CCommonLib::SafeReleaseCOM( &m_pCommandAllocator );
}
ID3D12CommandAllocator* CCommandAllocator::Handle()
{
    return m_pCommandAllocator;
}
void CCommandAllocator::Reset()
{
    m_pCommandAllocator->Reset();
}