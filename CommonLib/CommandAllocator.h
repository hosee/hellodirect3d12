//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CCommandAllocator
{
public:
    CCommandAllocator( ID3D12Device* pDevice ,D3D12_COMMAND_LIST_TYPE commandListType );
    ~CCommandAllocator();
    ID3D12CommandAllocator* Handle();
    void Reset();
private:
    ID3D12CommandAllocator* m_pCommandAllocator;
private:
    //Disable default constructor
    CCommandAllocator(){}
    CCommandAllocator(CCommandAllocator& copy){}
};