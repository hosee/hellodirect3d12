//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "CommandQueue.h"
CCommandQueue::CCommandQueue( ID3D12Device* pDevice , const D3D12_COMMAND_QUEUE_DESC& desc ) :
    m_pCommandQueue( nullptr )
{
    if( FAILED( pDevice->CreateCommandQueue( &desc, IID_ID3D12CommandQueue , (void**)&m_pCommandQueue ) ) )
        throw std::runtime_error("Create command queue was failed.");
}
CCommandQueue::~CCommandQueue()
{
    CCommonLib::SafeReleaseCOM( &m_pCommandQueue );
}
ID3D12CommandQueue* CCommandQueue::Handle()
{
    return m_pCommandQueue;
}
void CCommandQueue::AddCommandList(ID3D12CommandList* pCommandList)
{
    m_vecSubmitList.emplace_back( pCommandList );
}
size_t CCommandQueue::SubmitCommand()
{
    size_t submitAmount = m_vecSubmitList.size();
#ifdef _WIN64
    m_pCommandQueue->ExecuteCommandLists( (UINT)m_vecSubmitList.size(), m_vecSubmitList.data() );
#else
    m_pCommandQueue->ExecuteCommandLists( m_vecSubmitList.size(), m_vecSubmitList.data() );
#endif // _WIN64
    m_vecSubmitList.clear();
    return submitAmount;
}