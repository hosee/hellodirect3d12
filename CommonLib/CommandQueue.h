//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "vector"
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CCommandQueue
{
public:
    CCommandQueue( ID3D12Device* pDevice , const D3D12_COMMAND_QUEUE_DESC& desc );
    ~CCommandQueue();
    ID3D12CommandQueue* Handle();
    void AddCommandList(ID3D12CommandList* pCommandList);
    size_t SubmitCommand();
private:
    ID3D12CommandQueue* m_pCommandQueue;
    std::vector<ID3D12CommandList*> m_vecSubmitList;
private:
    //Disable default constructor
    CCommandQueue(){}
    CCommandQueue(CCommandQueue& copy){}
};