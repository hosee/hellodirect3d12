//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "CommittedResource.h"
CCommittedResource::CCommittedResource( ID3D12Device* pDevice , D3D12_HEAP_TYPE heapType ,  D3D12_RESOURCE_STATES initResourceState , UINT64 bufferSize ) :
    m_pResource( nullptr ),
    m_eHeapType( heapType ),
    m_eResourceDimension( D3D12_RESOURCE_DIMENSION_BUFFER ),
    m_uiBufferSize( bufferSize )
{
    CCommonLib::ThrowFailed( 
        pDevice->CreateCommittedResource(
            &CDirect3DUtil::MakeHeapProperties( m_eHeapType ),
            D3D12_HEAP_FLAG_NONE,
            &CDirect3DUtil::MakeResourceDesc_Buffer( bufferSize ),
            initResourceState,
            nullptr,
            IID_ID3D12Resource,
            (void**)&m_pResource),
        "Create committed resource was failed.");
}
CCommittedResource::CCommittedResource( 
    ID3D12Device* pDevice, 
    D3D12_HEAP_TYPE heapType,
    D3D12_RESOURCE_STATES initResourceState, 
    TypeTexture2D* pTypeCheck,
    DXGI_FORMAT format,
    UINT64 width,
    UINT height,
    UINT16 arraySize,
    UINT16 mipLevels,
    UINT sampleCount,
    UINT sampleQuality,
    D3D12_RESOURCE_FLAGS flags,
    D3D12_TEXTURE_LAYOUT layout,
    UINT64 alignment,
    const D3D12_CLEAR_VALUE* pOptimizedClearValue):
    m_pResource( nullptr ),
    m_eHeapType( heapType ),
    m_eResourceDimension( D3D12_RESOURCE_DIMENSION_TEXTURE2D )
{
    CCommonLib::ThrowFailed( 
        pDevice->CreateCommittedResource(
            &CDirect3DUtil::MakeHeapProperties( m_eHeapType ),
            D3D12_HEAP_FLAG_NONE,
            &CDirect3DUtil::MakeResourceDesc_Texture2D( 
                format,
                width,
                height,
                arraySize,
                mipLevels,
                sampleCount,
                sampleQuality,
                flags,
                layout,
                alignment),
            initResourceState,
            pOptimizedClearValue,
            IID_ID3D12Resource,
            (void**)&m_pResource),
        "Create committed resource was failed.");
}
CCommittedResource::~CCommittedResource()
{
    CCommonLib::SafeReleaseCOM( &m_pResource );
}
ID3D12Resource* CCommittedResource::Handle()
{
    return m_pResource;
}
D3D12_HEAP_TYPE CCommittedResource::HeapType()
{
    return m_eHeapType;
}
D3D12_RESOURCE_DIMENSION CCommittedResource::ResourceDimension()
{
    return m_eResourceDimension;
}
UINT64 CCommittedResource::BufferSize()
{
    return m_uiBufferSize;
}
D3D12_GPU_VIRTUAL_ADDRESS CCommittedResource::GetGPUVirtualAddress()
{
    return m_pResource->GetGPUVirtualAddress();
}
HRESULT CCommittedResource::Map( UINT subresource , const D3D12_RANGE* pReadRange , void** ppData )
{
    return m_pResource->Map( subresource , pReadRange , ppData );
}
void CCommittedResource::Unmap( UINT subresource , const D3D12_RANGE* pWrittenRange )
{
    m_pResource->Unmap( subresource , pWrittenRange );
}