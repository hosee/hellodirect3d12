//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
#include "Direct3DUtil.h"
class CCommittedResource
{
public:
    struct TypeTexture2D{};
public:
    CCommittedResource( ID3D12Device* pDevice , D3D12_HEAP_TYPE heapType , D3D12_RESOURCE_STATES initResourceState , UINT64 bufferSize );
    CCommittedResource( 
        ID3D12Device* pDevice, 
        D3D12_HEAP_TYPE heapType,
        D3D12_RESOURCE_STATES initResourceState, 
        TypeTexture2D* pTypeCheck,
        DXGI_FORMAT format,
        UINT64 width,
        UINT height,
        UINT16 arraySize = 1,
        UINT16 mipLevels = 0,
        UINT sampleCount = 1,
        UINT sampleQuality = 0,
        D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE,
        D3D12_TEXTURE_LAYOUT layout = D3D12_TEXTURE_LAYOUT_UNKNOWN,
        UINT64 alignment = 0 ,
        const D3D12_CLEAR_VALUE* pOptimizedClearValue = nullptr);
    ~CCommittedResource();
    ID3D12Resource* Handle();
    D3D12_HEAP_TYPE HeapType();
    D3D12_RESOURCE_DIMENSION ResourceDimension();
    UINT64 BufferSize();
    D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress();
    HRESULT Map( UINT subresource , const D3D12_RANGE* pReadRange , void** ppData );
    void Unmap( UINT subresource , const D3D12_RANGE* pWrittenRange );
private:
    ID3D12Resource* m_pResource;
    D3D12_HEAP_TYPE m_eHeapType;
    D3D12_RESOURCE_DIMENSION m_eResourceDimension;
    UINT64 m_uiBufferSize;
};