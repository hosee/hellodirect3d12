//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "CommonLib.h"
CCommonLib::CCommonLib()
{}
CCommonLib::~CCommonLib()
{}
void CCommonLib::ThrowFailed(HRESULT hr,const char* pComment)
{
    if(FAILED(hr) )
    { 
        std::string str = "HResult:" + std::to_string(hr) + "." + pComment;
        throw std::runtime_error( str.c_str() );
    }
}