//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "string"
#include "stdexcept"
//
#include "windows.h"
class CCommonLib
{
public:
    template< typename T >
    static void SafeReleaseObject( T** ppObj)
    {
        if( (*ppObj) != nullptr )
        {
            delete (*ppObj);
            (*ppObj) = nullptr;
        }
    }
    template< typename T >
    static void SafeReleaseCOM( T** ppCOM )
    {
        if( (*ppCOM) != nullptr)
        {
            (*ppCOM)->Release();
            (*ppCOM) = nullptr;
        }
    }
    static void ThrowFailed(HRESULT hr,const char* pComment);
public:
    ~CCommonLib();
private:
    CCommonLib();
};