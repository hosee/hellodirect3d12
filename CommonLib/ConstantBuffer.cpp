//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "ConstantBuffer.h"
CConstantBuffer::CConstantBuffer( ID3D12Device* pDevice , size_t bufferSize ) :
    m_pConstantBuffer( nullptr )
{
    if( bufferSize == 0  )
        throw std::runtime_error("Constant buffer size can't be zero.");
    //
    if( bufferSize > UINT32_MAX )
        throw std::runtime_error("Constant buffer need a lot of memories that big than max size.");
    //
    //Buffer size is required to 256 Bytes aligned.
    size_t tagBufferSize = ( bufferSize + 255 ) & ~255;
    m_pConstantBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , tagBufferSize );
    //
    m_stCBV.BufferLocation = m_pConstantBuffer->GetGPUVirtualAddress();
#if defined(_WIN64)
    m_stCBV.SizeInBytes = (UINT)tagBufferSize;
#else
    m_stCBV.SizeInBytes = tagBufferSize;
#endif
}
CConstantBuffer::~CConstantBuffer()
{
    CCommonLib::SafeReleaseObject( &m_pConstantBuffer );
}
const D3D12_CONSTANT_BUFFER_VIEW_DESC* CConstantBuffer::CBV()
{
    return &m_stCBV;
}
bool CConstantBuffer::WriteData( const uint8_t* pData , size_t dataSize , size_t offset)
{
    if( ( dataSize + offset ) > m_pConstantBuffer->BufferSize() )
        return false;
    //
    uint8_t* pConstantData;
    D3D12_RANGE readRange = { 0 , 0 };
    CCommonLib::ThrowFailed(
        m_pConstantBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pConstantData) ),
        "Map constant buffer was failed.");
    //
    memcpy( pConstantData + offset , pData, dataSize );
    m_pConstantBuffer->Unmap(0, nullptr);
    //
    return true;
}