//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "CommittedResource.h"
#include "GraphicsCommandList.h"
class CConstantBuffer
{
public:
    CConstantBuffer( ID3D12Device* pDevice , size_t bufferSize );
    ~CConstantBuffer();
    const D3D12_CONSTANT_BUFFER_VIEW_DESC* CBV();
    bool WriteData( const uint8_t* pData , size_t dataSize , size_t offset = 0 );
private:
    CCommittedResource* m_pConstantBuffer;
    D3D12_CONSTANT_BUFFER_VIEW_DESC m_stCBV;
};