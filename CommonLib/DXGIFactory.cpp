//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "DXGIFactory.h"
CDXGIFactory::CDXGIFactory( bool enabledDebugMode ) :
    m_pDXGIFactory( nullptr ),
    m_pD3D12Debug( nullptr )
{
    UINT dxgiFactoryFlags = 0;
    if( enabledDebugMode )
    {
        if( SUCCEEDED( D3D12GetDebugInterface( IID_ID3D12Debug , (void**)&m_pD3D12Debug ) ) )
        {
            m_pD3D12Debug->EnableDebugLayer();
            //
            dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
        }
        else
          throw std::runtime_error("Enable debug mode in CDXGIFactory.");
    }
    //
    if( FAILED( CreateDXGIFactory2( dxgiFactoryFlags, IID_IDXGIFactory2 , (void**)&m_pDXGIFactory ) ) )
    {
        CCommonLib::SafeReleaseCOM( &m_pD3D12Debug );
        throw std::runtime_error("Create DXGIFactory was failed.");
    }
    //
    IDXGIAdapter1* pAdapter = nullptr;

    for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != m_pDXGIFactory->EnumAdapters1(adapterIndex, &pAdapter); ++adapterIndex)
    {
        DXGI_ADAPTER_DESC1 desc;
        pAdapter->GetDesc1( &desc );
        //
        //Ignored software adapter
        if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
        {
          CCommonLib::SafeReleaseCOM( &pAdapter );
          continue;
        }
        //
        m_vecAdapterList.push_back(pAdapter);
    }
    //
}
CDXGIFactory::~CDXGIFactory()
{
    for( size_t i = 0 ; i < m_vecAdapterList.size() ; i++)
      CCommonLib::SafeReleaseCOM( &m_vecAdapterList[i] );
    //
    m_vecAdapterList.clear();
    //
    CCommonLib::SafeReleaseCOM( &m_pDXGIFactory );
    CCommonLib::SafeReleaseCOM( &m_pD3D12Debug );
}
IDXGIFactory4* CDXGIFactory::Handle()
{
    return m_pDXGIFactory;
}
size_t CDXGIFactory::AdapterAmount() const
{
    return m_vecAdapterList.size();
}
IDXGIAdapter1* CDXGIFactory::GetAdapterHandle( size_t adapterIndex)
{
    if( adapterIndex >= m_vecAdapterList.size() )
        return nullptr;
    //
    return m_vecAdapterList[ adapterIndex ];
}