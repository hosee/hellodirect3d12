//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "vector"
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CDXGIFactory
{
public:
    CDXGIFactory( bool enabledDebugMode );
    ~CDXGIFactory();
    IDXGIFactory4* Handle();
    size_t AdapterAmount() const;
    IDXGIAdapter1* GetAdapterHandle( size_t adapterIndex);
private:
    IDXGIFactory4* m_pDXGIFactory;
    ID3D12Debug* m_pD3D12Debug;
    std::vector< IDXGIAdapter1* > m_vecAdapterList;
private:
    //Disable default constructor
    CDXGIFactory(){}
    CDXGIFactory(CDXGIFactory& copy){}
};