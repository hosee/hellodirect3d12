//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "DepthStencilBuffer.h"
CDepthStencilBuffer::CDepthStencilBuffer( ID3D12Device* pDevice , DXGI_FORMAT format , UINT width , UINT height , float clearDepth , UINT8 clearStencil ):
    m_pDepthStencilBuffer( nullptr ),
    m_fClearDepth( clearDepth ),
    m_uiClearStencil( clearStencil )
{
    D3D12_CLEAR_VALUE clearValue = {};
    clearValue.Format = format;
    clearValue.DepthStencil.Depth = m_fClearDepth;
    clearValue.DepthStencil.Stencil = m_uiClearStencil;
    //
    m_pDepthStencilBuffer = new CCommittedResource( 
        pDevice,
        D3D12_HEAP_TYPE_DEFAULT,
        D3D12_RESOURCE_STATE_DEPTH_WRITE,
        (CCommittedResource::TypeTexture2D*)nullptr,
        format,
        width,
        height,
        1,
        0,
        1,
        0,
        D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL,
        D3D12_TEXTURE_LAYOUT_UNKNOWN,
        0,
        &clearValue);
    //
    m_stDSV.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
    m_stDSV.Format = format;
    m_stDSV.Flags = D3D12_DSV_FLAG_NONE;
    //
}
CDepthStencilBuffer::~CDepthStencilBuffer()
{
    CCommonLib::SafeReleaseObject( &m_pDepthStencilBuffer );
}
ID3D12Resource* CDepthStencilBuffer::Handle()
{
    return m_pDepthStencilBuffer->Handle();
}
const D3D12_DEPTH_STENCIL_VIEW_DESC* CDepthStencilBuffer::DSV()
{
    return &m_stDSV;
}
float CDepthStencilBuffer::ClearDepth()
{
    return m_fClearDepth;
}
UINT8 CDepthStencilBuffer::ClearStencil()
{
    return m_uiClearStencil;
}