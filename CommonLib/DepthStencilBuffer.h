//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "CommittedResource.h"
#include "GraphicsCommandList.h"
class CDepthStencilBuffer
{
public:
    CDepthStencilBuffer( ID3D12Device* pDevice , DXGI_FORMAT format , UINT width , UINT height , float clearDepth = 1.0f, UINT8 clearStencil = 0 );
    ~CDepthStencilBuffer();
    ID3D12Resource* Handle();
    const D3D12_DEPTH_STENCIL_VIEW_DESC* DSV();
    float ClearDepth();
    UINT8 ClearStencil();
private:
    CCommittedResource* m_pDepthStencilBuffer;
    D3D12_DEPTH_STENCIL_VIEW_DESC m_stDSV;
    float m_fClearDepth;
    UINT8 m_uiClearStencil;
};
