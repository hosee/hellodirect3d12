//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "DescriptorHeap.h"
CDescriptorHeap::CDescriptorHeap( ID3D12Device* pDevice , D3D12_DESCRIPTOR_HEAP_TYPE eType , UINT descriptorAmount , D3D12_DESCRIPTOR_HEAP_FLAGS flags ):
    m_pDevice( pDevice ),
    m_pDescriptorHeap( nullptr ),
    m_eType( eType ),
    m_uiDescriptorAmount( descriptorAmount ),
    m_uiDescriptorHandleIncrementSize( 0 )
{
    D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
    heapDesc.NumDescriptors = m_uiDescriptorAmount;
    heapDesc.Type = m_eType;
    heapDesc.Flags = flags;
    CCommonLib::ThrowFailed(
      m_pDevice->CreateDescriptorHeap( &heapDesc , IID_ID3D12DescriptorHeap , (void**)&m_pDescriptorHeap ),
      "Create descriptor heap was failed."
    );
    //
    m_uiDescriptorHandleIncrementSize=m_pDevice->GetDescriptorHandleIncrementSize(m_eType);
}
CDescriptorHeap::~CDescriptorHeap()
{
    CCommonLib::SafeReleaseCOM( &m_pDescriptorHeap );
}
ID3D12DescriptorHeap* CDescriptorHeap::Handle()
{
    return m_pDescriptorHeap;
}
D3D12_DESCRIPTOR_HEAP_TYPE CDescriptorHeap::Type()
{
    return m_eType;
}
UINT CDescriptorHeap::DescriptorAmount()
{
    return m_uiDescriptorAmount;
}
bool CDescriptorHeap::WriteConstantBufferView( UINT index , const D3D12_CONSTANT_BUFFER_VIEW_DESC* pCBVDesc )
{
    if( m_eType != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
        return false;
    //
    if( index >= m_uiDescriptorAmount )
        return false;
    //
    D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    handle.ptr += index * m_uiDescriptorHandleIncrementSize;
    m_pDevice->CreateConstantBufferView( pCBVDesc , handle);
    return true;
}
bool CDescriptorHeap::WriteShaderResourceView( UINT index , ID3D12Resource* pTexture, const D3D12_SHADER_RESOURCE_VIEW_DESC* pSRVDesc )
{
    if( m_eType != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
        return false;
    //
    if( index >= m_uiDescriptorAmount )
        return false;
    //
    D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    handle.ptr += index * m_uiDescriptorHandleIncrementSize;
    m_pDevice->CreateShaderResourceView( pTexture , pSRVDesc , handle);
    return true;
}
bool CDescriptorHeap::WriteSampler( UINT index , const D3D12_SAMPLER_DESC* pSamplerDesc)
{
    if( m_eType != D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER)
        return false;
    //
    if( index >= m_uiDescriptorAmount )
        return false;
    //
    D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    handle.ptr += index * m_uiDescriptorHandleIncrementSize;
    m_pDevice->CreateSampler( pSamplerDesc , handle);
    return true;
}
bool CDescriptorHeap::WriteRenderTargetView( UINT index , ID3D12Resource* pTexture , const D3D12_RENDER_TARGET_VIEW_DESC* pRTVDesc )
{
    if( m_eType != D3D12_DESCRIPTOR_HEAP_TYPE_RTV)
        return false;
    //
    if( index >= m_uiDescriptorAmount )
        return false;
    //
    D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    handle.ptr += index * m_uiDescriptorHandleIncrementSize;
    m_pDevice->CreateRenderTargetView( pTexture , pRTVDesc , handle);
    return true;
}
bool CDescriptorHeap::WriteDepthStencilView( UINT index , ID3D12Resource* pTexture , const D3D12_DEPTH_STENCIL_VIEW_DESC* pDSVDesc )
{
    if( m_eType != D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
        return false;
    //
    if( index >= m_uiDescriptorAmount )
        return false;
    //
    D3D12_CPU_DESCRIPTOR_HANDLE handle = m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
    handle.ptr += index * m_uiDescriptorHandleIncrementSize;
    m_pDevice->CreateDepthStencilView( pTexture , pDSVDesc , handle);
    return true;
}
D3D12_CPU_DESCRIPTOR_HANDLE CDescriptorHeap::GetCPUDescriptorHandleForHeapStart()
{
  return m_pDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
}
D3D12_GPU_DESCRIPTOR_HANDLE CDescriptorHeap::GetGPUDescriptorHandle( UINT index )
{
  if(index >= m_uiDescriptorAmount)
  {
    D3D12_GPU_DESCRIPTOR_HANDLE nullHandle = { 0 };
    return nullHandle;
  } 
  D3D12_GPU_DESCRIPTOR_HANDLE tagHandle = m_pDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
  tagHandle.ptr += index * m_uiDescriptorHandleIncrementSize;
  return tagHandle;
}