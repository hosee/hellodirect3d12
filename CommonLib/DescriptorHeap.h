//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CDescriptorHeap
{
public:
    CDescriptorHeap( ID3D12Device* pDevice , D3D12_DESCRIPTOR_HEAP_TYPE eType , UINT descriptorAmount , D3D12_DESCRIPTOR_HEAP_FLAGS flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE );
    ~CDescriptorHeap();
    ID3D12DescriptorHeap* Handle();
    D3D12_DESCRIPTOR_HEAP_TYPE Type();
    UINT DescriptorAmount();
    bool WriteConstantBufferView( UINT index , const D3D12_CONSTANT_BUFFER_VIEW_DESC* pCBVDesc );
    bool WriteShaderResourceView( UINT index , ID3D12Resource* pTexture, const D3D12_SHADER_RESOURCE_VIEW_DESC* pSRVDesc );
    bool WriteSampler( UINT index , const D3D12_SAMPLER_DESC* pSamplerDesc);
    bool WriteRenderTargetView( UINT index , ID3D12Resource* pTexture , const D3D12_RENDER_TARGET_VIEW_DESC* pRTVDesc );
    bool WriteDepthStencilView( UINT index , ID3D12Resource* pTexture , const D3D12_DEPTH_STENCIL_VIEW_DESC* pDSVDesc );
    D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart();
    D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle( UINT index );
private:
    ID3D12Device* m_pDevice;
    ID3D12DescriptorHeap* m_pDescriptorHeap;
    D3D12_DESCRIPTOR_HEAP_TYPE m_eType;
    UINT m_uiDescriptorAmount;
    UINT m_uiDescriptorHandleIncrementSize;
};