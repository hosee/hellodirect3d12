//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Device.h"
CDevice::CDevice(IDXGIAdapter1* pDXGIAdapter) :
    m_pDevice(nullptr)
{
  if(FAILED( D3D12CreateDevice( pDXGIAdapter , D3D_FEATURE_LEVEL_11_0 , IID_ID3D12Device , (void**)&m_pDevice ) ) )
      throw std::runtime_error("Create device was failed.");
}
CDevice::~CDevice()
{
    CCommonLib::SafeReleaseCOM( &m_pDevice );
}
ID3D12Device* CDevice::Handle()
{
    return m_pDevice;
}