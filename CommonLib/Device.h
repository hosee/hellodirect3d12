//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CDevice
{
    ID3D12Device* m_pDevice;
public:
    CDevice(IDXGIAdapter1* pDXGIAdapter);
    ~CDevice();
    ID3D12Device* Handle();
private:
    //Disable default constructor
    CDevice(){}
    CDevice(CDevice& copy){}
};
