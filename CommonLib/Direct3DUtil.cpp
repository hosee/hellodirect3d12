//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Direct3DUtil.h"
const D3D12_RASTERIZER_DESC CDirect3DUtil::s_stDefaultRasterizerDesc = {
    D3D12_FILL_MODE_SOLID,//FillMode
    D3D12_CULL_MODE_BACK,//CullMode
    FALSE,//FrontCounterClockwise
    D3D12_DEFAULT_DEPTH_BIAS,//DepthBias
    D3D12_DEFAULT_DEPTH_BIAS_CLAMP,//DepthBiasClamp
    D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS,//SlopeScaledDepthBias
    TRUE,//DepthClipEnable
    FALSE,//MultisampleEnable
    FALSE,//AntialiasedLineEnable
    0,//ForcedSampleCount
    D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF,//ConservativeRaster
};
const D3D12_RENDER_TARGET_BLEND_DESC CDirect3DUtil::s_stDefaultRenderTargetBlendDesc = {
    FALSE,//BlendEnable
    FALSE,//LogicOpEnable
    D3D12_BLEND_ONE,//SrcBlend
    D3D12_BLEND_ZERO,//DestBlend
    D3D12_BLEND_OP_ADD,//BlendOp
    D3D12_BLEND_ONE,//SrcBlendAlpha
    D3D12_BLEND_ZERO,//DestBlendAlpha
    D3D12_BLEND_OP_ADD,//BlendOpAlpha
    D3D12_LOGIC_OP_NOOP,//LogicOp
    D3D12_COLOR_WRITE_ENABLE_ALL,//RenderTargetWriteMask
};
const D3D12_BLEND_DESC CDirect3DUtil::s_stDefaultBlendDesc = {
    FALSE,//AlphaToCoverageEnable
    FALSE,//IndependentBlendEnable
    {
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
      s_stDefaultRenderTargetBlendDesc,
    },//RenderTarget[8]
};
const D3D12_DEPTH_STENCIL_DESC CDirect3DUtil::s_stDefaultDepthStencilDesc = {
    FALSE,//DepthEnable
    D3D12_DEPTH_WRITE_MASK_ZERO,//DepthWriteMask
    D3D12_COMPARISON_FUNC_NEVER,//DepthFunc
    FALSE,//StencilEnable
    0,//StencilReadMask
    0,//StencilWriteMask
    {
        D3D12_STENCIL_OP_KEEP,//StencilFailOp
        D3D12_STENCIL_OP_KEEP,//StencilDepthFailOp
        D3D12_STENCIL_OP_KEEP,//StencilPassOp
        D3D12_COMPARISON_FUNC_NEVER,//StencilFunc
    },//FrontFace
    {
        D3D12_STENCIL_OP_KEEP,//StencilFailOp
        D3D12_STENCIL_OP_KEEP,//StencilDepthFailOp
        D3D12_STENCIL_OP_KEEP,//StencilPassOp
        D3D12_COMPARISON_FUNC_NEVER,//StencilFunc
    },//BackFace
};
CDirect3DUtil::CDirect3DUtil()
{}
CDirect3DUtil::~CDirect3DUtil()
{}
std::wstring CDirect3DUtil::GetAdapterInfo( IDXGIAdapter1* pAdapter )
{
    DXGI_ADAPTER_DESC1 desc;
    pAdapter->GetDesc1( &desc );
    return desc.Description;
}
D3D12_HEAP_PROPERTIES CDirect3DUtil::MakeHeapProperties( D3D12_HEAP_TYPE heapType , UINT creationNodeMask , UINT nodeMask )
{
    D3D12_HEAP_PROPERTIES tagHeapProperties = {};
    tagHeapProperties.Type = heapType;
    tagHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    tagHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    tagHeapProperties.CreationNodeMask = creationNodeMask;
    tagHeapProperties.VisibleNodeMask = nodeMask;
    //
    return tagHeapProperties;
}
D3D12_RESOURCE_DESC CDirect3DUtil::MakeResourceDesc_Buffer( UINT64 width , D3D12_RESOURCE_FLAGS flags , UINT64 alignment )
{
    D3D12_RESOURCE_DESC tagDesc = {};
    tagDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    tagDesc.Alignment = alignment;
    tagDesc.Width = width;
    tagDesc.Height = 1;
    tagDesc.DepthOrArraySize = 1;
    tagDesc.MipLevels = 1;
    tagDesc.Format = DXGI_FORMAT_UNKNOWN;
    tagDesc.SampleDesc.Count = 1;
    tagDesc.SampleDesc.Quality = 0;
    tagDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    tagDesc.Flags = flags;
    //
    return tagDesc;
}
D3D12_RESOURCE_DESC CDirect3DUtil::MakeResourceDesc_Texture2D( 
    DXGI_FORMAT format,
    UINT64 width,
    UINT height,
    UINT16 arraySize,
    UINT16 mipLevels,
    UINT sampleCount,
    UINT sampleQuality,
    D3D12_RESOURCE_FLAGS flags,
    D3D12_TEXTURE_LAYOUT layout,
    UINT64 alignment)
{
    D3D12_RESOURCE_DESC tagDesc = {};
    tagDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    tagDesc.Alignment = alignment;
    tagDesc.Width = width;
    tagDesc.Height = height;
    tagDesc.DepthOrArraySize = arraySize;
    tagDesc.MipLevels = mipLevels;
    tagDesc.Format = format;
    tagDesc.SampleDesc.Count = 1;
    tagDesc.SampleDesc.Quality = 0;
    tagDesc.Layout = layout;
    tagDesc.Flags = flags;
    //
    return tagDesc;
}
D3D12_DESCRIPTOR_RANGE1 CDirect3DUtil::MakeDescriptorRange( 
    D3D12_DESCRIPTOR_RANGE_TYPE rangeType,
    UINT numDescriptors,
    UINT baseShaderRegister,
    UINT registerSpace,
    D3D12_DESCRIPTOR_RANGE_FLAGS flags,
    UINT offsetInDescriptorsFromTableStart)
{
    D3D12_DESCRIPTOR_RANGE1 tagRange = {};
    tagRange.RangeType = rangeType;
    tagRange.NumDescriptors = numDescriptors;
    tagRange.BaseShaderRegister = baseShaderRegister;
    tagRange.RegisterSpace = registerSpace;
    tagRange.Flags = flags;
    tagRange.OffsetInDescriptorsFromTableStart = offsetInDescriptorsFromTableStart;
    //
    return tagRange;
}
D3D12_ROOT_PARAMETER1 CDirect3DUtil::MakeRootParameter_DescriptorTable(
    UINT numDescriptorRanges,
    const D3D12_DESCRIPTOR_RANGE1* pDescriptorRanges,
    D3D12_SHADER_VISIBILITY visibility)
{
    D3D12_ROOT_PARAMETER1 tagParemeter = {};
    tagParemeter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    tagParemeter.ShaderVisibility = visibility;
    tagParemeter.DescriptorTable.pDescriptorRanges = pDescriptorRanges;
    tagParemeter.DescriptorTable.NumDescriptorRanges = numDescriptorRanges;
    //
    return tagParemeter;
}
D3D12_STATIC_SAMPLER_DESC CDirect3DUtil::MakeStaticSamplerDesc(
    D3D12_FILTER filter,
    D3D12_TEXTURE_ADDRESS_MODE addressU,
    D3D12_TEXTURE_ADDRESS_MODE addressV,
    D3D12_TEXTURE_ADDRESS_MODE addressW,
    UINT shaderRegister,
    UINT registerSpace,
    D3D12_SHADER_VISIBILITY shaderVisibility,
    FLOAT mipLODBias,
    UINT maxAnisotropy,
    D3D12_COMPARISON_FUNC comparisonFunc,
    D3D12_STATIC_BORDER_COLOR  borderColor,
    FLOAT minLOD,
    FLOAT maxLOD)
{
    D3D12_STATIC_SAMPLER_DESC tagDesc = {};
    tagDesc.Filter = filter;
    tagDesc.AddressU = addressU;
    tagDesc.AddressV = addressV;
    tagDesc.AddressW = addressW;
    tagDesc.MipLODBias = mipLODBias;
    tagDesc.MaxAnisotropy = maxAnisotropy;
    tagDesc.ComparisonFunc = comparisonFunc;
    tagDesc.BorderColor = borderColor;
    tagDesc.MinLOD = minLOD;
    tagDesc.MaxLOD = maxLOD;
    tagDesc.ShaderRegister = shaderRegister;
    tagDesc.RegisterSpace = registerSpace;
    tagDesc.ShaderVisibility = shaderVisibility;
    //
    return tagDesc;
}
bool CDirect3DUtil::GenerateCheckerBoardData( size_t width , size_t height , std::vector<UINT8>* pTagData )
{
    if( width == 0 || height == 0 || pTagData == nullptr )
        return false;
    //

    UINT rowPitch = ( (width * 4) + 255 ) & ~255;
    UINT textureBufferSize = ( ( rowPitch * height ) + 511 ) & ~511;

    (*pTagData).resize( textureBufferSize );
    UINT8* pData = &(*pTagData)[0];
    memset( pData , 0 , textureBufferSize);
    for( UINT i = 0;i < height;i++ )
    {
        UINT8* pRowData = pData + ( i * rowPitch );
        for( UINT j = 0;j < width;j++ )
        {
            UINT8* pPixelData = pRowData + ( j * 4 );
            if( ( (i / 64) % 2 ) == ( (j / 64) % 2 ) )
            {
                pPixelData[ 0 ] = 0x00;
                pPixelData[ 1 ] = 0x00;
                pPixelData[ 2 ] = 0x00;
                pPixelData[ 3 ] = 0xff;
            }
            else
            {
                pPixelData[ 0 ] = 0xff;
                pPixelData[ 1 ] = 0xff;
                pPixelData[ 2 ] = 0xff;
                pPixelData[ 3 ] = 0xff;
            }
        }
        //
    }

    return true;
}