//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//STL
#include "string"
#include "stdexcept"
#include "vector"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CDirect3DUtil
{
public:
    static const D3D12_RASTERIZER_DESC s_stDefaultRasterizerDesc;
    static const D3D12_RENDER_TARGET_BLEND_DESC s_stDefaultRenderTargetBlendDesc;
    static const D3D12_BLEND_DESC s_stDefaultBlendDesc;
    static const D3D12_DEPTH_STENCIL_DESC s_stDefaultDepthStencilDesc;
public:
    static std::wstring GetAdapterInfo( IDXGIAdapter1* pAdapter );
    static D3D12_HEAP_PROPERTIES MakeHeapProperties( D3D12_HEAP_TYPE heapType , UINT creationNodeMask = 1, UINT nodeMask = 1);
    static D3D12_RESOURCE_DESC MakeResourceDesc_Buffer( UINT64 width , D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE , UINT64 alignment = 0 );
    static D3D12_RESOURCE_DESC MakeResourceDesc_Texture2D( 
        DXGI_FORMAT format,
        UINT64 width,
        UINT height,
        UINT16 arraySize = 1,
        UINT16 mipLevels = 0,
        UINT sampleCount = 1,
        UINT sampleQuality = 0,
        D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE,
        D3D12_TEXTURE_LAYOUT layout = D3D12_TEXTURE_LAYOUT_UNKNOWN,
        UINT64 alignment = 0 );
    static D3D12_DESCRIPTOR_RANGE1 MakeDescriptorRange( 
        D3D12_DESCRIPTOR_RANGE_TYPE rangeType,
        UINT numDescriptors,
        UINT baseShaderRegister,
        UINT registerSpace = 0,
        D3D12_DESCRIPTOR_RANGE_FLAGS flags = D3D12_DESCRIPTOR_RANGE_FLAG_NONE,
        UINT offsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND);
    static D3D12_ROOT_PARAMETER1 MakeRootParameter_DescriptorTable(
        UINT numDescriptorRanges,
        const D3D12_DESCRIPTOR_RANGE1* pDescriptorRanges,
        D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
    static D3D12_STATIC_SAMPLER_DESC MakeStaticSamplerDesc(
        D3D12_FILTER filter,
        D3D12_TEXTURE_ADDRESS_MODE addressU,
        D3D12_TEXTURE_ADDRESS_MODE addressV,
        D3D12_TEXTURE_ADDRESS_MODE addressW,
        UINT shaderRegister,
        UINT registerSpace,
        D3D12_SHADER_VISIBILITY shaderVisibility,
        FLOAT mipLODBias = 0.0f,
        UINT maxAnisotropy = 0,
        D3D12_COMPARISON_FUNC comparisonFunc = D3D12_COMPARISON_FUNC_NEVER,
        D3D12_STATIC_BORDER_COLOR  borderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK,
        FLOAT minLOD = 0.0f,
        FLOAT maxLOD = D3D12_FLOAT32_MAX );
    //
    static bool GenerateCheckerBoardData( size_t width , size_t height , std::vector<UINT8>* pTagData );
public:
    ~CDirect3DUtil();
private:
    CDirect3DUtil();
};