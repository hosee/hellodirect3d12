//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "FPSController.h"
CFPSController::CFPSController(float frameTime ) :
    m_fFPSModefiyTime( 0.0f ),
    m_fFrameTime( frameTime )
{}
CFPSController::~CFPSController()
{}
float CFPSController::FrameTime()
{
    return m_fFrameTime;
}
void CFPSController::FrameTime(float time)
{
    if( time <= 0.0f )
        return;
    //
    m_fFrameTime = time;
}
void CFPSController::Reset()
{
    m_cTimer.Reset();
    m_fFPSModefiyTime = 0.0f;
}
void CFPSController::UpdateAndSleep()
{
    float sleepTime = m_fFrameTime - ( ( (float)m_cTimer.DeltaTime() ) + m_fFPSModefiyTime );
    if( sleepTime > 0.001f)
        ::Sleep( (DWORD)(sleepTime*1000) );
    //
    float executeTime = ( (float)m_cTimer.DeltaTime() ) + m_fFPSModefiyTime;
    m_fFPSModefiyTime = ( executeTime < m_fFrameTime ) ? ( m_fFrameTime - executeTime ): 0.0f;
    m_cTimer.Reset();
}