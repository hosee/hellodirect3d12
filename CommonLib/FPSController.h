//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "windows.h"
#include "Timer.h"
class CFPSController
{
public:
    CFPSController(float frameTime = 0.0167);
    ~CFPSController();
    float FrameTime();
    void FrameTime(float time);
    void Reset();
    void UpdateAndSleep();
private:
    CTimer m_cTimer;
    float m_fFPSModefiyTime;
    float m_fFrameTime;
};