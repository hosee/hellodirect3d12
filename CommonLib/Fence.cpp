//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Fence.h"
CFence::CFence( ID3D12Device* pDevice ) :
    m_pFence( nullptr ),
    m_hFenceEvent( 0 ),
    m_uiFenceValue( 0 )
{
    if( FAILED(pDevice->CreateFence( 0 , D3D12_FENCE_FLAG_NONE , IID_ID3D12Fence , (void**)&m_pFence ) ) )
        throw std::runtime_error("Create fence was failed.");
    //
    m_hFenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
    if( m_hFenceEvent == 0)
        throw std::runtime_error("Create fence event was failed.");
}
CFence::~CFence()
{
    CloseHandle( m_hFenceEvent );
    CCommonLib::SafeReleaseCOM( &m_pFence );
}
ID3D12Fence* CFence::Handle()
{
    return m_pFence;
}
HANDLE CFence::EventHandle()
{
    return m_hFenceEvent;
}
void CFence::WaitFenceCompleted( ID3D12CommandQueue* pCommandQueue , UINT64 waitCon )
{
    if( waitCon == 0)
        return;
    //
    if(waitCon >= UINT32_MAX)
    {
        m_pFence->Signal(0);
        m_uiFenceValue = 0;
    }
    //
    UINT64 tagFenceValue = m_uiFenceValue + waitCon;
    pCommandQueue->Signal( m_pFence , tagFenceValue );
    m_uiFenceValue+=waitCon;
    if( m_pFence->GetCompletedValue() < tagFenceValue )
    {
        m_pFence->SetEventOnCompletion( tagFenceValue , m_hFenceEvent );
        WaitForSingleObject( m_hFenceEvent , INFINITE );
    }
    //
    if(m_uiFenceValue >= UINT32_MAX)
    {
        m_pFence->Signal(0);
        m_uiFenceValue = 0;
    }
}