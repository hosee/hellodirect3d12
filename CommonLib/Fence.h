
//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CFence
{
public:
    CFence( ID3D12Device* pDevice );
    ~CFence();
    ID3D12Fence* Handle();
    HANDLE EventHandle();
    void WaitFenceCompleted( ID3D12CommandQueue* pCommandQueue , UINT64 waitCon = 1);
private:
    ID3D12Fence* m_pFence;
    HANDLE m_hFenceEvent;
    UINT64 m_uiFenceValue;
};