//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "GraphicsCommandList.h"
CGraphicsCommandList::CGraphicsCommandList( ID3D12Device* pDevice , ID3D12CommandAllocator* pCommandAllocator , D3D12_COMMAND_LIST_TYPE commandListType ) :
    m_pGraphicsCommandList( nullptr )
{
    if( FAILED( pDevice->CreateCommandList( 0 , commandListType , pCommandAllocator , nullptr ,IID_ID3D12GraphicsCommandList , (void**)&m_pGraphicsCommandList ) ) )
        throw std::runtime_error("Create graphics command list was failed.");
    //
    m_pGraphicsCommandList->Close();
}
CGraphicsCommandList::~CGraphicsCommandList()
{
    CCommonLib::SafeReleaseCOM( &m_pGraphicsCommandList );
}
ID3D12GraphicsCommandList* CGraphicsCommandList::Handle()
{
    return m_pGraphicsCommandList;
}
void CGraphicsCommandList::Reset( ID3D12CommandAllocator* pCommandAllocator )
{
    m_pGraphicsCommandList->Reset( pCommandAllocator , nullptr );
}
void CGraphicsCommandList::Close()
{
    m_pGraphicsCommandList->Close();
}
void CGraphicsCommandList::BarrierTransition(
    ID3D12Resource* pResource,
    D3D12_RESOURCE_STATES stateBefore,
    D3D12_RESOURCE_STATES stateAfter,
    UINT subresource,
    D3D12_RESOURCE_BARRIER_FLAGS flags)
{
    D3D12_RESOURCE_BARRIER barrier = {};
    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = flags;
    barrier.Transition.pResource = pResource;
    barrier.Transition.StateBefore = stateBefore;
    barrier.Transition.StateAfter = stateAfter;
    barrier.Transition.Subresource = subresource;
    //
    m_pGraphicsCommandList->ResourceBarrier( 1 , &barrier );
}
void CGraphicsCommandList::CopyBufferRegion( ID3D12Resource* pDstBuffer , UINT64 dstOffset , ID3D12Resource* pSrcBuffer , UINT64 srcOffset , UINT64 numBytes )
{
    m_pGraphicsCommandList->CopyBufferRegion( pDstBuffer , dstOffset , pSrcBuffer , srcOffset , numBytes );
}
void CGraphicsCommandList::CopyTextureRegion( 
    const D3D12_TEXTURE_COPY_LOCATION* pDst, 
    UINT dstX, 
    UINT dstY, 
    UINT dstZ, 
    const D3D12_TEXTURE_COPY_LOCATION* pSrc, 
    const D3D12_BOX* pSrcBox)
{
    m_pGraphicsCommandList->CopyTextureRegion( pDst , dstX , dstY , dstZ , pSrc , pSrcBox );
}
void CGraphicsCommandList::ClearRenderTargetView(D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle , const float* pColor , UINT numRects, const D3D12_RECT* pRects)
{
    m_pGraphicsCommandList->ClearRenderTargetView( rtvHandle , pColor , numRects , pRects );
}
void CGraphicsCommandList::ClearDepthStencilView( 
    D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle, 
    D3D12_CLEAR_FLAGS clearFlags, 
    FLOAT depth, 
    UINT8 stencil, 
    UINT numRects, 
    const D3D12_RECT* pRects)
{
    m_pGraphicsCommandList->ClearDepthStencilView( dsvHandle , clearFlags , depth , stencil , numRects , pRects );
}
void CGraphicsCommandList::RSSetViewports( UINT numViewports , const D3D12_VIEWPORT* pViewports)
{
    m_pGraphicsCommandList->RSSetViewports( numViewports , pViewports );
}
void CGraphicsCommandList::RSSetScissorRects( UINT numRects , const D3D12_RECT* pRects )
{
    m_pGraphicsCommandList->RSSetScissorRects( numRects , pRects );
}
void CGraphicsCommandList::OMSetRenderTargets( 
    UINT numRenderTargetDescriptors , 
    const D3D12_CPU_DESCRIPTOR_HANDLE* pRenderTargetDescriptors , 
    BOOL rtsSingleHandleToDescriptorRange,
    const D3D12_CPU_DESCRIPTOR_HANDLE* pDepthStencilDescriptor)
{
    m_pGraphicsCommandList->OMSetRenderTargets( numRenderTargetDescriptors , pRenderTargetDescriptors , rtsSingleHandleToDescriptorRange ,pDepthStencilDescriptor);
}
void CGraphicsCommandList::SetDescriptorHeaps( ID3D12DescriptorHeap* pHeap )
{
  ID3D12DescriptorHeap* pHeapList[] = { pHeap };
  m_pGraphicsCommandList->SetDescriptorHeaps( 1 , pHeapList );
}
void CGraphicsCommandList::SetDescriptorHeaps(ID3D12DescriptorHeap* pHeap0,ID3D12DescriptorHeap* pHeap1 )
{
  ID3D12DescriptorHeap* pHeapList[] = { pHeap0 , pHeap1 };
  m_pGraphicsCommandList->SetDescriptorHeaps( 2 , pHeapList );
}
void CGraphicsCommandList::SetGraphicsRootSignature( ID3D12RootSignature* pRootSignature )
{
    m_pGraphicsCommandList->SetGraphicsRootSignature( pRootSignature );
}
void CGraphicsCommandList::IASetVertexBuffers( UINT startSlot , UINT numViews , const D3D12_VERTEX_BUFFER_VIEW* pViews )
{
    m_pGraphicsCommandList->IASetVertexBuffers( startSlot , numViews , pViews );
}
void CGraphicsCommandList::SetPipelineState( ID3D12PipelineState* pPipelineState )
{
    m_pGraphicsCommandList->SetPipelineState( pPipelineState );
}
void CGraphicsCommandList::SetGraphicsRootDescriptorTable( UINT rootParameterIndex , D3D12_GPU_DESCRIPTOR_HANDLE baseDescriptor )
{
    m_pGraphicsCommandList->SetGraphicsRootDescriptorTable( rootParameterIndex , baseDescriptor );
}
void CGraphicsCommandList::IASetPrimitiveTopology( D3D12_PRIMITIVE_TOPOLOGY primitiveTopology )
{
    m_pGraphicsCommandList->IASetPrimitiveTopology( primitiveTopology );
}
void CGraphicsCommandList::IASetIndexBuffer( const D3D12_INDEX_BUFFER_VIEW* pView )
{
    m_pGraphicsCommandList->IASetIndexBuffer( pView );
}
void CGraphicsCommandList::DrawInstanced( UINT vertexCountPerInstance , UINT instanceCount , UINT startVertexLocation , UINT startInstanceLocation )
{
    m_pGraphicsCommandList->DrawInstanced( vertexCountPerInstance , instanceCount , startVertexLocation , startInstanceLocation );
}
void CGraphicsCommandList::DrawIndexedInstanced( UINT indexCountPerInstance, UINT instanceCount , UINT startIndexLocation , INT baseVertexLocation , UINT startInstanceLocation )
{
    m_pGraphicsCommandList->DrawIndexedInstanced( indexCountPerInstance , instanceCount , startIndexLocation , baseVertexLocation , startInstanceLocation );
}