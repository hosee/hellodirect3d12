//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CGraphicsCommandList
{
public:
    CGraphicsCommandList( ID3D12Device* pDevice , ID3D12CommandAllocator* pCommandAllocator ,D3D12_COMMAND_LIST_TYPE commandListType );
    ~CGraphicsCommandList();
    ID3D12GraphicsCommandList* Handle();
    void Reset( ID3D12CommandAllocator* pCommandAllocator );
    void Close();
    void BarrierTransition(
        ID3D12Resource* pResource,
        D3D12_RESOURCE_STATES stateBefore,
        D3D12_RESOURCE_STATES stateAfter,
        UINT subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
        D3D12_RESOURCE_BARRIER_FLAGS flags = D3D12_RESOURCE_BARRIER_FLAG_NONE);
    void CopyBufferRegion( ID3D12Resource* pDstBuffer , UINT64 dstOffset , ID3D12Resource* pSrcBuffer , UINT64 srcOffset , UINT64 numBytes );
    void CopyTextureRegion( 
        const D3D12_TEXTURE_COPY_LOCATION* pDst, 
        UINT dstX, 
        UINT dstY, 
        UINT dstZ, 
        const D3D12_TEXTURE_COPY_LOCATION* pSrc, 
        const D3D12_BOX* pSrcBox);
    void ClearRenderTargetView( D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle , const float* pColor , UINT numRects = 0 , const D3D12_RECT* pRects = nullptr );
    void ClearDepthStencilView( D3D12_CPU_DESCRIPTOR_HANDLE dsvHandle , D3D12_CLEAR_FLAGS clearFlags , FLOAT depth , UINT8 stencil , UINT numRects = 0 , const D3D12_RECT* pRects = nullptr );
    void RSSetViewports( UINT numViewports , const D3D12_VIEWPORT* pViewports );
    void RSSetScissorRects( UINT numRects , const D3D12_RECT* pRects );
    void OMSetRenderTargets( 
        UINT numRenderTargetDescriptors , 
        const D3D12_CPU_DESCRIPTOR_HANDLE* pRenderTargetDescriptors , 
        BOOL rtsSingleHandleToDescriptorRange,
        const D3D12_CPU_DESCRIPTOR_HANDLE* pDepthStencilDescriptor);
    void SetDescriptorHeaps( ID3D12DescriptorHeap* pHeap );
    void SetDescriptorHeaps( ID3D12DescriptorHeap* pHeap0 , ID3D12DescriptorHeap* pHeap1 );
    void SetGraphicsRootSignature( ID3D12RootSignature* pRootSignature );
    void IASetVertexBuffers( UINT startSlot , UINT numViews , const D3D12_VERTEX_BUFFER_VIEW* pViews );
    void SetPipelineState( ID3D12PipelineState* pPipelineState );
    void SetGraphicsRootDescriptorTable( UINT rootParameterIndex , D3D12_GPU_DESCRIPTOR_HANDLE baseDescriptor );
    void IASetPrimitiveTopology( D3D12_PRIMITIVE_TOPOLOGY primitiveTopology );
    void IASetIndexBuffer( const D3D12_INDEX_BUFFER_VIEW* pView );
    void DrawInstanced( UINT vertexCountPerInstance , UINT instanceCount , UINT startVertexLocation , UINT startInstanceLocation );
    void DrawIndexedInstanced( UINT indexCountPerInstance, UINT instanceCount , UINT startIndexLocation , INT baseVertexLocation , UINT startInstanceLocation );
private:
    ID3D12GraphicsCommandList* m_pGraphicsCommandList;
private:
    //Disable default constructor
    CGraphicsCommandList(){}
    CGraphicsCommandList(CGraphicsCommandList& copy){}
};