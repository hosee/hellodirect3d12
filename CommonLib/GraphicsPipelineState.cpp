//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "GraphicsPipelineState.h"
CGraphicsPipelineState::CGraphicsPipelineState( ID3D12Device* pDevice , const D3D12_GRAPHICS_PIPELINE_STATE_DESC* pDesc ) :
    m_pGraphicsPipelineState( nullptr )
{
    CCommonLib::ThrowFailed(
        pDevice->CreateGraphicsPipelineState(
            pDesc,
            IID_ID3D12PipelineState,
            (void**)&m_pGraphicsPipelineState),
        "Create graphics pipeline state was failed.");
}
CGraphicsPipelineState::~CGraphicsPipelineState()
{
    CCommonLib::SafeReleaseCOM( &m_pGraphicsPipelineState );
}
ID3D12PipelineState* CGraphicsPipelineState::Handle()
{
    return m_pGraphicsPipelineState;
}