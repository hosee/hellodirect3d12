//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CGraphicsPipelineState
{
public:
    CGraphicsPipelineState( ID3D12Device* pDevice , const D3D12_GRAPHICS_PIPELINE_STATE_DESC* pDesc );
    ~CGraphicsPipelineState();
    ID3D12PipelineState* Handle();
private:
    ID3D12PipelineState* m_pGraphicsPipelineState;
};
