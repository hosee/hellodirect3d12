//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "IndexBuffer.h"
CIndexBuffer::CIndexBuffer( ID3D12Device* pDevice , size_t indexAmount , DXGI_FORMAT indexFormat , bool needStagingBuffer ) :
    m_eIndexFormat( indexFormat ),
    m_szIndexAmount( indexAmount ),
    m_pIndexBuffer( nullptr ),
    m_pStagingBuffer( nullptr ),
    m_bNeedSubmitStagingBuffer( false )
{
    size_t indexElementSize;
    switch( m_eIndexFormat )
    {
    case DXGI_FORMAT_R16_UINT:
      indexElementSize = sizeof( uint16_t );
      break;
    case DXGI_FORMAT_R32_UINT:
      indexElementSize = sizeof( uint32_t );
      break;
    default:
      indexElementSize = 0;
      break;
    }
    if( indexElementSize == 0)
        throw std::runtime_error("Unknown index buffer format.");
    //
    if( m_szIndexAmount == 0)
        throw std::runtime_error("Index amount can't be zero.");
    //
    size_t bufferSize = indexElementSize * indexAmount;
    if( bufferSize > UINT32_MAX )
        throw std::runtime_error("Index buffer need a lot of memories that big than max size.");
    //
    if( needStagingBuffer )
    {
        m_pIndexBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_DEFAULT , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
        m_pStagingBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
    }
    else
    {
        m_pIndexBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
    }
    //
    m_stIBV.BufferLocation = m_pIndexBuffer->GetGPUVirtualAddress();
    m_stIBV.Format = m_eIndexFormat;
#if defined(_WIN64)
    m_stIBV.SizeInBytes = (UINT)bufferSize;
#else
    m_stIBV.SizeInBytes = bufferSize;
#endif
}
CIndexBuffer::~CIndexBuffer()
{
    CCommonLib::SafeReleaseObject( &m_pStagingBuffer );
    CCommonLib::SafeReleaseObject( &m_pIndexBuffer );
}
DXGI_FORMAT CIndexBuffer::Format()
{
    return m_eIndexFormat;
}
size_t CIndexBuffer::IndexAmount()
{
    return m_szIndexAmount;
}
const D3D12_INDEX_BUFFER_VIEW* CIndexBuffer::IBV()
{
    return &m_stIBV;
}
bool CIndexBuffer::WriteData( const uint8_t* pData , size_t dataSize , size_t offset )
{
    //
    if( ( dataSize + offset ) > m_pIndexBuffer->BufferSize() )
        return false;
    //
    if( m_pStagingBuffer != nullptr )
    {
        uint8_t* pVertexData;
        D3D12_RANGE readRange = { 0 , 0 };
        CCommonLib::ThrowFailed(
            m_pStagingBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexData) ),
            "Map index buffer was failed.");
        //
        memcpy( pVertexData + offset , pData, dataSize );
        m_pStagingBuffer->Unmap(0, nullptr);
        m_bNeedSubmitStagingBuffer = true;
    }
    else
    {
        uint8_t* pVertexData;
        D3D12_RANGE readRange = { 0 , 0 };
        CCommonLib::ThrowFailed(
            m_pIndexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexData) ),
            "Map index buffer was failed.");
        //
        memcpy( pVertexData + offset , pData, dataSize );
        m_pIndexBuffer->Unmap(0, nullptr);
    }
    //
    return true;
}
bool CIndexBuffer::NeedSubmitStagingBuffer()
{
    return m_bNeedSubmitStagingBuffer;
}
bool CIndexBuffer::SubmitStagingBuffer(CGraphicsCommandList* pCommandList)
{
    if( pCommandList == nullptr)
        return false;
    if( m_bNeedSubmitStagingBuffer )
    {
      //
      pCommandList->BarrierTransition( m_pIndexBuffer->Handle() , D3D12_RESOURCE_STATE_GENERIC_READ , D3D12_RESOURCE_STATE_COPY_DEST );
      //
      pCommandList->CopyBufferRegion( m_pIndexBuffer->Handle() , 0 , m_pStagingBuffer->Handle() , 0 , m_pStagingBuffer->BufferSize() );
      //
      pCommandList->BarrierTransition( m_pIndexBuffer->Handle() , D3D12_RESOURCE_STATE_COPY_DEST , D3D12_RESOURCE_STATE_GENERIC_READ );
      //
      m_bNeedSubmitStagingBuffer = false;
    }
    return true;
}