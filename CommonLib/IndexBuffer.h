//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "CommittedResource.h"
#include "GraphicsCommandList.h"
class CIndexBuffer
{
public:
    CIndexBuffer( ID3D12Device* pDevice , size_t indexAmount , DXGI_FORMAT indexFormat = DXGI_FORMAT_R32_UINT , bool needStagingBuffer = true );
    ~CIndexBuffer();
    DXGI_FORMAT Format();
    size_t IndexAmount();
    const D3D12_INDEX_BUFFER_VIEW* IBV();
    bool WriteData( const uint8_t* pData , size_t dataSize , size_t offset = 0 );
    bool NeedSubmitStagingBuffer();
    bool SubmitStagingBuffer(CGraphicsCommandList* pCommandList);
private:
    DXGI_FORMAT m_eIndexFormat;
    size_t m_szIndexAmount;
    CCommittedResource* m_pIndexBuffer;
    CCommittedResource* m_pStagingBuffer;
    D3D12_INDEX_BUFFER_VIEW m_stIBV;
    bool m_bNeedSubmitStagingBuffer;
};
