//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "MathUtil.h"
CMathUtil::CMathUtil()
{}
CMathUtil::~CMathUtil()
{}
glm::mat4x4 CMathUtil::PerspectiveMatrixRad( float radians , float aspect , float near , float far )
{
    return glm::perspective( radians , aspect , near , far );
}
glm::mat4x4 CMathUtil::PerspectiveMatrixDeg( float degree , float aspect , float near , float far )
{
    return PerspectiveMatrixRad( glm::radians( degree ) , aspect , near , far );
}
glm::mat4x4 CMathUtil::ViewMatrix( glm::vec3 position , glm::quat qua )
{
    return glm::translate( glm::mat4( 1.0f ) , glm::vec3( -position.x, -position.y , -position.z ) ) * glm::transpose( glm::toMat4( qua ) );
}
glm::mat4x4 CMathUtil::WorldMatrix( glm::vec3 position , glm::quat qua , glm::vec3 scale )
{
    return 
        glm::translate( glm::mat4( 1.0f ) , position ) *
        glm::toMat4( qua ) *
        glm::scale( glm::mat4( 1.0f ) , scale );
}