//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
//glm
#include "glm/matrix.hpp"
#include "glm/gtc/matrix_transform.hpp"
//#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/quaternion.hpp"
//
class CMathUtil
{
public:
    static glm::mat4x4 PerspectiveMatrixRad( float radians , float aspect , float near , float far );
    static glm::mat4x4 PerspectiveMatrixDeg( float degree , float aspect , float near , float far );
    static glm::mat4x4 ViewMatrix( glm::vec3 position , glm::quat qua );
    static glm::mat4x4 WorldMatrix( glm::vec3 position , glm::quat qua , glm::vec3 scale );
public:
    ~CMathUtil();
private:
    CMathUtil();
};
