//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "RootSignature.h"
CRootSignature::CRootSignature( ID3D12Device* pDevice , const D3D12_ROOT_SIGNATURE_DESC* pDesc , D3D_ROOT_SIGNATURE_VERSION version) :
    m_pRootSignature( nullptr )
{
    ID3DBlob* pSignatureBlob = nullptr;
    ID3DBlob* pErrorMsgBlob = nullptr;
    //
    HRESULT hr = D3D12SerializeRootSignature( pDesc , version , &pSignatureBlob, &pErrorMsgBlob );
    if( FAILED(hr) )
    {
        ::OutputDebugStringA( (LPCSTR)pErrorMsgBlob->GetBufferPointer() );
        pErrorMsgBlob->Release();
        throw std::runtime_error("Serialize root signature was failed.");
    }
    //
    CCommonLib::ThrowFailed(
        pDevice->CreateRootSignature( 
            0, 
            pSignatureBlob->GetBufferPointer(), 
            pSignatureBlob->GetBufferSize(), 
            IID_ID3D12RootSignature, 
            (void**)&m_pRootSignature ),
        "Create root signature was failed."
    );
}
CRootSignature::CRootSignature( ID3D12Device* pDevice , const D3D12_ROOT_SIGNATURE_DESC1* pDesc , D3D_ROOT_SIGNATURE_VERSION version ):
    m_pRootSignature( nullptr )
{
    ID3DBlob* pSignatureBlob = nullptr;
    ID3DBlob* pErrorMsgBlob = nullptr;
    //
    D3D12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc = {};
    rootSignatureDesc.Version = version;
    rootSignatureDesc.Desc_1_1 = *pDesc;
    HRESULT hr = D3D12SerializeVersionedRootSignature( &rootSignatureDesc , &pSignatureBlob, &pErrorMsgBlob );
    if( FAILED(hr) )
    {
        ::OutputDebugStringA( (LPCSTR)pErrorMsgBlob->GetBufferPointer() );
        pErrorMsgBlob->Release();
        throw std::runtime_error("Serialize root signature was failed.");
    }
    //
    CCommonLib::ThrowFailed(
        pDevice->CreateRootSignature( 
            0, 
            pSignatureBlob->GetBufferPointer(), 
            pSignatureBlob->GetBufferSize(), 
            IID_ID3D12RootSignature, 
            (void**)&m_pRootSignature ),
        "Create root signature was failed."
    );
}
CRootSignature::~CRootSignature()
{
    CCommonLib::SafeReleaseCOM( &m_pRootSignature );
}
ID3D12RootSignature* CRootSignature::Handle()
{
    return m_pRootSignature;
}