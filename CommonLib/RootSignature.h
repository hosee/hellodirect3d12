//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CRootSignature
{
public:
    CRootSignature( ID3D12Device* pDevice , const D3D12_ROOT_SIGNATURE_DESC* pDesc , D3D_ROOT_SIGNATURE_VERSION version );
    CRootSignature( ID3D12Device* pDevice , const D3D12_ROOT_SIGNATURE_DESC1* pDesc , D3D_ROOT_SIGNATURE_VERSION version );
    ~CRootSignature();
    ID3D12RootSignature* Handle();
private:
    ID3D12RootSignature* m_pRootSignature;
};