//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "SwapChain.h"
CSwapChain::CSwapChain(
    IDXGIFactory4* pDXGIFactory,
    ID3D12Device* pDevice,
    ID3D12CommandQueue* pCommandQueue,
    HWND hWnd,
    UINT width,
    UINT height,
    DXGI_FORMAT format,
    UINT frameCon) :
    m_pSwapChain( nullptr ),
    m_pRTVHeap( nullptr ),
    m_uiBackBufferIndex( 0 ),
    m_uiRTVDescriptorSize( 0 )
{
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
    swapChainDesc.BufferCount = frameCon;
    swapChainDesc.Width = width;
    swapChainDesc.Height = height;
    swapChainDesc.Format = format;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
    swapChainDesc.SampleDesc.Count = 1;
    if( FAILED( pDXGIFactory->CreateSwapChainForHwnd( pCommandQueue , hWnd , &swapChainDesc ,nullptr , nullptr , (IDXGISwapChain1**)&m_pSwapChain ) ) )
      throw std::runtime_error("Create swap chain was failed.");
    //
    m_uiBackBufferIndex = m_pSwapChain->GetCurrentBackBufferIndex();
    //
    D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
    rtvHeapDesc.NumDescriptors = frameCon;
    rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    CCommonLib::ThrowFailed(
        pDevice->CreateDescriptorHeap( 
            &rtvHeapDesc, 
            IID_ID3D12DescriptorHeap, 
            (void**)&m_pRTVHeap),
        "Create descriptor heap was failed in CSwapChain.");
    //
    D3D12_CPU_DESCRIPTOR_HANDLE descriptorHandle = m_pRTVHeap->GetCPUDescriptorHandleForHeapStart();
    m_uiRTVDescriptorSize = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    for(UINT i=0;i<frameCon;i++)
    {
        ID3D12Resource* pCurSwapChainBuffer = nullptr;
        if( FAILED( m_pSwapChain->GetBuffer( i , IID_ID3D12Resource , (void**)&pCurSwapChainBuffer ) ) )
            throw std::runtime_error("Swap chain get buffer has error.");
        //
        pDevice->CreateRenderTargetView( pCurSwapChainBuffer , nullptr, descriptorHandle );
        //
        m_vecSwapChainBufferList.emplace_back(pCurSwapChainBuffer);
        descriptorHandle.ptr += m_uiRTVDescriptorSize;
    }
}
CSwapChain::~CSwapChain()
{
    //
    for(size_t i=0;i<m_vecSwapChainBufferList.size();i++)
        CCommonLib::SafeReleaseCOM(&m_vecSwapChainBufferList[i]);
    //
    m_vecSwapChainBufferList.clear();
    //
    CCommonLib::SafeReleaseCOM( &m_pRTVHeap );
    CCommonLib::SafeReleaseCOM( &m_pSwapChain );
}
IDXGISwapChain3* CSwapChain::Handle()
{
    return m_pSwapChain;
}
ID3D12Resource* CSwapChain::Buffer( size_t index )
{
    if( index >= m_vecSwapChainBufferList.size() )
        return nullptr;
    //
    return m_vecSwapChainBufferList[ index ];
}
size_t CSwapChain::BackBufferIndex()
{
    return m_uiBackBufferIndex;
}
void CSwapChain::Present()
{
    m_pSwapChain->Present( 0 , 0 );
}
void CSwapChain::UpdateBackBufferIndex()
{
    m_uiBackBufferIndex = m_pSwapChain->GetCurrentBackBufferIndex();
}
bool CSwapChain::GetBackBufferDescriptorHandle( size_t index , D3D12_CPU_DESCRIPTOR_HANDLE& refHandle)
{
    if(index >= m_vecSwapChainBufferList.size() )
        return false;
    //
    refHandle.ptr = m_pRTVHeap->GetCPUDescriptorHandleForHeapStart().ptr + ( index * m_uiRTVDescriptorSize);

    return true;
}