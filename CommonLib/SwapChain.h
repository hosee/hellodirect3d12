//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "vector"
#include "stdexcept"
//
#include "d3d12.h"
#include "dxgi1_4.h"
#include "D3Dcompiler.h"
//
#include "CommonLib.h"
class CSwapChain
{
public:
    CSwapChain( 
      IDXGIFactory4* pDXGIFactory ,
      ID3D12Device* pDevice,
      ID3D12CommandQueue* pCommandQueue,
      HWND hWnd,
      UINT width,
      UINT height,
      DXGI_FORMAT format,
      UINT frameCon = 2 );
    ~CSwapChain();
    IDXGISwapChain3* Handle();
    ID3D12Resource* Buffer( size_t index );
    size_t BackBufferIndex();
    void Present();
    void UpdateBackBufferIndex();
    bool GetBackBufferDescriptorHandle( size_t index , D3D12_CPU_DESCRIPTOR_HANDLE& refHandle);
private:
    IDXGISwapChain3* m_pSwapChain;
    ID3D12DescriptorHeap* m_pRTVHeap;
    std::vector<ID3D12Resource*> m_vecSwapChainBufferList;
    UINT m_uiBackBufferIndex;
    UINT m_uiRTVDescriptorSize;
private:
    //Disable default constructor
    CSwapChain(){}
    CSwapChain(CSwapChain& copy){}
};