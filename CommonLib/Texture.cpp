//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Texture.h"
CTexture::CTexture( 
    ID3D12Device* pDevice, 
    TypeTexture2D* pTypeCheck,
    DXGI_FORMAT format,
    UINT64 width,
    UINT height,
    UINT16 arraySize,
    UINT16 mipLevels,
    UINT sampleCount,
    UINT sampleQuality,
    D3D12_RESOURCE_FLAGS flags,
    D3D12_TEXTURE_LAYOUT layout,
    UINT64 alignment):
    m_pTexture( nullptr ),
    m_pStagingBuffer( nullptr ),
    m_bNeedSubmitStagingBuffer( false ),
    m_eFormat( format )
{
    //
    m_pTexture = new CCommittedResource( 
        pDevice, 
        D3D12_HEAP_TYPE_DEFAULT, 
        D3D12_RESOURCE_STATE_GENERIC_READ,
        (CCommittedResource::TypeTexture2D*)nullptr,
        format,
        width,
        height,
        arraySize,
        mipLevels,
        sampleCount,
        sampleQuality,
        flags,
        layout,
        alignment);
    //
    UINT64 stageBufferSize;
    if( !GenerateSubresourceCopyInfo( format , width , height , arraySize , mipLevels , &stageBufferSize) )
        throw std::runtime_error("CTexture generate subresource copy info was failed.");
    //
    m_pStagingBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , stageBufferSize); 
    //
    m_stSRV.Format = format;
    m_stSRV.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
    m_stSRV.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    m_stSRV.Texture2D.MipLevels = (mipLevels) ? mipLevels : 1;
    m_stSRV.Texture2D.MostDetailedMip = 0;
    m_stSRV.Texture2D.PlaneSlice = 0;
    m_stSRV.Texture2D.ResourceMinLODClamp = 0.0f;
    //
}
CTexture::~CTexture()
{
    CCommonLib::SafeReleaseObject( &m_pStagingBuffer );
    CCommonLib::SafeReleaseObject( &m_pTexture );
}
ID3D12Resource* CTexture::Handle()
{
    return m_pTexture->Handle();
}
const D3D12_SHADER_RESOURCE_VIEW_DESC* CTexture::SRV()
{
    return &m_stSRV;
}
bool CTexture::WriteData( const uint8_t* pData , size_t dataSize , size_t subresourceIndex )
{
  UINT64 bufferSize = CalSubresourceBufferSize( subresourceIndex );
  if( bufferSize == 0 || dataSize > bufferSize )
      return false;
  //
  uint8_t* pTextureData;
  D3D12_RANGE readRange = { 0 , 0 };
  CCommonLib::ThrowFailed(
      m_pStagingBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pTextureData) ),
      "Map texture staging buffer was failed.");
  //
  memcpy( pTextureData + m_cSubresourceCopyInfoList[ subresourceIndex ].CopyOffset , pData, dataSize );
  m_pStagingBuffer->Unmap(0, nullptr);
  //
  m_bNeedSubmitStagingBuffer = true;
  return true;
}
bool CTexture::NeedSubmitStagingBuffer()
{
  return m_bNeedSubmitStagingBuffer;
}
bool CTexture::SubmitStagingBuffer(CGraphicsCommandList* pCommandList)
{
  if( pCommandList == nullptr)
      return false;
  //
  if( m_bNeedSubmitStagingBuffer )
  {
      //
      pCommandList->BarrierTransition( m_pTexture->Handle() , D3D12_RESOURCE_STATE_GENERIC_READ , D3D12_RESOURCE_STATE_COPY_DEST );
      //
      D3D12_TEXTURE_COPY_LOCATION tagLoc = {};
      tagLoc.pResource = m_pTexture->Handle();
      tagLoc.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
      //
      D3D12_TEXTURE_COPY_LOCATION srcLoc = {};
      srcLoc.pResource = m_pStagingBuffer->Handle();
      srcLoc.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
      srcLoc.PlacedFootprint.Footprint.Format = m_eFormat;
      //
      for( UINT i = 0;i < m_cSubresourceCopyInfoList.size();i++ )
      {
          tagLoc.SubresourceIndex = i;
          //
          srcLoc.PlacedFootprint.Offset = m_cSubresourceCopyInfoList[i].CopyOffset;
          srcLoc.PlacedFootprint.Footprint.Format = m_eFormat;
          srcLoc.PlacedFootprint.Footprint.Width = m_cSubresourceCopyInfoList[i].Width;
          srcLoc.PlacedFootprint.Footprint.Height = m_cSubresourceCopyInfoList[i].Height;
          srcLoc.PlacedFootprint.Footprint.Depth = 1;
          srcLoc.PlacedFootprint.Footprint.RowPitch = m_cSubresourceCopyInfoList[i].RowPitch;
          //
          pCommandList->CopyTextureRegion( &tagLoc , 0 , 0 , 0 , &srcLoc , nullptr );
      }
      //
      pCommandList->BarrierTransition( m_pTexture->Handle() , D3D12_RESOURCE_STATE_COPY_DEST , D3D12_RESOURCE_STATE_GENERIC_READ );
      //
      m_bNeedSubmitStagingBuffer = false;
  }
  return true;
}
UINT CTexture::GetPixelSize(DXGI_FORMAT format)
{
    UINT tagSize;
    switch(format)
    {
    case DXGI_FORMAT_R8G8B8A8_TYPELESS:
    case DXGI_FORMAT_R8G8B8A8_UNORM:
    case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
    case DXGI_FORMAT_R8G8B8A8_UINT:
    case DXGI_FORMAT_R8G8B8A8_SNORM:
    case DXGI_FORMAT_R8G8B8A8_SINT:
        tagSize = 4;
        break;
    default:
        tagSize = 0;
        break;
    }
    //
    return tagSize;
}
bool CTexture::GenerateSubresourceCopyInfo( DXGI_FORMAT format , UINT64 width , UINT height , UINT16 arraySize , UINT16 mipLevels , UINT64* pTagBufferSize )
{
    SubresourceCopyInfo tempInfo = {};
    UINT16 tagMipLevels = ( mipLevels != 0 ) ? mipLevels : 1;
    UINT16 tagSubresourceAmount = arraySize * tagMipLevels;
    if( tagSubresourceAmount == 0 )
        return false;
    //
    UINT pixelSize = GetPixelSize( format );
    if( pixelSize == 0 )
        return false;
    //
    UINT64 curbufferSize = 0;
    for( UINT16 i = 0;i < arraySize;i++ )
    {
        UINT curWidth = (UINT)width;
        UINT curHeight = height;
        for(UINT j = 0;j < tagMipLevels;j++ )
        {
            tempInfo.CopyOffset = curbufferSize;
            tempInfo.Width = curWidth;
            tempInfo.Height = curHeight;
            tempInfo.RowPitch = ( (curWidth * pixelSize ) + 255 ) & ~255;
            //
            curbufferSize += ( (tempInfo.RowPitch * curHeight) + 511 ) & ~511;
            m_cSubresourceCopyInfoList.emplace_back( tempInfo );
            //
            curWidth /= 2; 
            curHeight /= 2;
        }
    }
    //
    if( pTagBufferSize != nullptr )
        (*pTagBufferSize) = curbufferSize;
    //
    return true;
}
UINT64 CTexture::CalSubresourceBufferSize( size_t subreourceIndex )
{
  if( subreourceIndex >= m_cSubresourceCopyInfoList.size() )
      return 0;
  //
  return ( (m_cSubresourceCopyInfoList[ subreourceIndex ].RowPitch * m_cSubresourceCopyInfoList[ subreourceIndex ].Height) + 511 ) & ~511;
}