//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "vector"
#include "CommittedResource.h"
#include "GraphicsCommandList.h"
class CTexture
{
public:
    struct TypeTexture2D{};
public:
    CTexture( 
        ID3D12Device* pDevice, 
        TypeTexture2D* pTypeCheck,
        DXGI_FORMAT format,
        UINT64 width,
        UINT height,
        UINT16 arraySize = 1,
        UINT16 mipLevels = 0,
        UINT sampleCount = 1,
        UINT sampleQuality = 0,
        D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE,
        D3D12_TEXTURE_LAYOUT layout = D3D12_TEXTURE_LAYOUT_UNKNOWN,
        UINT64 alignment = 0);
    ~CTexture();
    ID3D12Resource* Handle();
    const D3D12_SHADER_RESOURCE_VIEW_DESC* SRV();
    bool WriteData( const uint8_t* pData , size_t dataSize , size_t subresourceIndex );
    bool NeedSubmitStagingBuffer();
    bool SubmitStagingBuffer(CGraphicsCommandList* pCommandList);
private:
    UINT GetPixelSize(DXGI_FORMAT format);
    bool GenerateSubresourceCopyInfo( DXGI_FORMAT format , UINT64 width , UINT height , UINT16 arraySize , UINT16 mipLevels , UINT64* pTagBufferSize);
    UINT64 CalSubresourceBufferSize( size_t subreourceIndex );
private:
    struct SubresourceCopyInfo
    {
        UINT64 CopyOffset;
        UINT Width;
        UINT Height;
        UINT RowPitch;
    };
private:
    CCommittedResource* m_pTexture;
    CCommittedResource* m_pStagingBuffer;
    D3D12_SHADER_RESOURCE_VIEW_DESC m_stSRV;
    bool m_bNeedSubmitStagingBuffer;
    DXGI_FORMAT m_eFormat;
    std::vector<SubresourceCopyInfo> m_cSubresourceCopyInfoList;
};
