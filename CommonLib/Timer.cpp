//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "Timer.h"
CTimer::CTimer()
{
  QueryPerformanceFrequency(&m_sFrequency);
  Reset();
}
CTimer::~CTimer()
{}
void CTimer::Reset()
{
  QueryPerformanceCounter(&m_sBaseCounter);
}
double CTimer::DeltaTime()
{
  LARGE_INTEGER curCounter;
  QueryPerformanceCounter(&curCounter);
  //
  double delCounter = ( double )( curCounter.QuadPart - m_sBaseCounter.QuadPart );
  double frequency = ( double )m_sFrequency.QuadPart;
  return delCounter / frequency;
}