//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "windows.h"
class CTimer
{
public:
    CTimer();
    ~CTimer();
    void Reset();
    double DeltaTime();
private:
    LARGE_INTEGER m_sFrequency;
    LARGE_INTEGER m_sBaseCounter;
};