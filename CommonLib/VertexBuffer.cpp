//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "VertexBuffer.h"
CVertexBuffer::CVertexBuffer( ID3D12Device* pDevice , size_t vertexStrideSize , size_t bufferSize , bool needStagingBuffer ) :
    m_pVertexBuffer( nullptr ),
    m_pStagingBuffer( nullptr ),
    m_bNeedSubmitStagingBuffer( false )
{
    if( bufferSize == 0  )
        throw std::runtime_error("Vertex buffer size can't be zero.");
    //
    if( bufferSize > UINT32_MAX )
        throw std::runtime_error("Vertex buffer need a lot of memories that big than max size.");
    //
    if( needStagingBuffer )
    {
        m_pVertexBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_DEFAULT , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
        m_pStagingBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
    }
    else
    {
        m_pVertexBuffer = new CCommittedResource( pDevice , D3D12_HEAP_TYPE_UPLOAD , D3D12_RESOURCE_STATE_GENERIC_READ , bufferSize );
    }
    //
    m_stVBV.BufferLocation = m_pVertexBuffer->GetGPUVirtualAddress();
#if defined(_WIN64)
    m_stVBV.StrideInBytes = (UINT)vertexStrideSize;
    m_stVBV.SizeInBytes = (UINT)bufferSize;
#else
    m_stVBV.StrideInBytes = vertexStrideSize;
    m_stVBV.SizeInBytes = bufferSize;
#endif
    
}
CVertexBuffer::~CVertexBuffer()
{
    CCommonLib::SafeReleaseObject( &m_pStagingBuffer );
    CCommonLib::SafeReleaseObject( &m_pVertexBuffer );
}
const D3D12_VERTEX_BUFFER_VIEW* CVertexBuffer::VBV()
{
    return &m_stVBV;
}
bool CVertexBuffer::WriteData( const uint8_t* pData , size_t dataSize , size_t offset )
{
    //
    if( ( dataSize + offset ) > m_pVertexBuffer->BufferSize() )
        return false;
    //
    if( m_pStagingBuffer != nullptr )
    {
        uint8_t* pVertexData;
        D3D12_RANGE readRange = { 0 , 0 };
        CCommonLib::ThrowFailed(
            m_pStagingBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexData) ),
            "Map vertex buffer was failed.");
        //
        memcpy( pVertexData + offset , pData, dataSize );
        m_pStagingBuffer->Unmap(0, nullptr);
        m_bNeedSubmitStagingBuffer = true;
    }
    else
    {
        uint8_t* pVertexData;
        D3D12_RANGE readRange = { 0 , 0 };
        CCommonLib::ThrowFailed(
            m_pVertexBuffer->Map(0, &readRange, reinterpret_cast<void**>(&pVertexData) ),
            "Map vertex buffer was failed.");
        //
        memcpy( pVertexData + offset , pData, dataSize );
        m_pVertexBuffer->Unmap(0, nullptr);
    }
    //
    return true;
}
bool CVertexBuffer::NeedSubmitStagingBuffer()
{
    return m_bNeedSubmitStagingBuffer;
}
bool CVertexBuffer::SubmitStagingBuffer(CGraphicsCommandList* pCommandList)
{
    if( pCommandList == nullptr)
        return false;
    if( m_bNeedSubmitStagingBuffer )
    {
      //
      pCommandList->BarrierTransition( m_pVertexBuffer->Handle() , D3D12_RESOURCE_STATE_GENERIC_READ , D3D12_RESOURCE_STATE_COPY_DEST );
      //
      pCommandList->CopyBufferRegion( m_pVertexBuffer->Handle() , 0 , m_pStagingBuffer->Handle() , 0 , m_pStagingBuffer->BufferSize() );
      //
      pCommandList->BarrierTransition( m_pVertexBuffer->Handle() , D3D12_RESOURCE_STATE_COPY_DEST , D3D12_RESOURCE_STATE_GENERIC_READ );
      //
      m_bNeedSubmitStagingBuffer = false;
    }
    return true;
}