//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "CommittedResource.h"
#include "GraphicsCommandList.h"
class CVertexBuffer
{
public:
    CVertexBuffer( ID3D12Device* pDevice , size_t vertexStrideSize ,size_t bufferSize , bool needStagingBuffer = true );
    ~CVertexBuffer();
    const D3D12_VERTEX_BUFFER_VIEW* VBV();
    bool WriteData( const uint8_t* pData , size_t dataSize , size_t offset = 0 );
    bool NeedSubmitStagingBuffer();
    bool SubmitStagingBuffer(CGraphicsCommandList* pCommandList);
private:
    CCommittedResource* m_pVertexBuffer;
    CCommittedResource* m_pStagingBuffer;
    D3D12_VERTEX_BUFFER_VIEW m_stVBV;
    bool m_bNeedSubmitStagingBuffer;
};