//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "glm/matrix.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "Application.h"
#include "Direct3DUtil.h"
#include "MathUtil.h"
#include "DXGIFactory.h"
#include "Device.h"
#include "CommandQueue.h"
#include "Fence.h"
#include "CommandAllocator.h"
#include "GraphicsCommandList.h"
#include "SwapChain.h"
#include "RootSignature.h"
#include "GraphicsPipelineState.h"
#include "DescriptorHeap.h"
#include "CommittedResource.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "ConstantBuffer.h"
#include "Texture.h"
#include "DepthStencilBuffer.h"
class CHello3DWorld : public AApplication
{
public:
    CHello3DWorld( HINSTANCE hInstance );
    virtual ~CHello3DWorld();
protected:
    HWND CreateDefaultWindow();
    virtual void OnInit() override;
    virtual void OnUpdate() override;
private:
    static const wchar_t* s_pProjectName;
    static const uint32_t s_uiDefaultWindowWidth;
    static const uint32_t s_uiDefaultWindowHeight;
private:
    struct Vertex
    {
        glm::vec4 position;
        //glm::vec4 color;
        glm::vec2 uv;
    };
    struct ConstantBuffer
    {
        glm::mat4x4 matWVP;
    };
private:
    CDXGIFactory* m_pDXGIFactory;
    CDevice* m_pDevice;
    CCommandQueue* m_pCommandQueue;
    CFence* m_pFence;
    CCommandAllocator* m_pCommandAllocator;
    CGraphicsCommandList* m_pGraphicsCommandList;
    CSwapChain* m_pSwapChain;
    CRootSignature* m_pRootSignature;
    CGraphicsPipelineState* m_pGraphicsPipelineState;
    CDescriptorHeap* m_pRTVHeap;
    CDescriptorHeap* m_pDSVHeap;
    CVertexBuffer* m_pVertexBuffer;
    CIndexBuffer* m_pIndexBuffer;
    CDescriptorHeap* m_pCSUHeap;
    CConstantBuffer* m_pConstantBuffer;
    CTexture* m_pTexture;
    CDepthStencilBuffer* m_pDepthStencilBuffer;
    CVertexBuffer* m_pVertexBufferTriangle;
    CIndexBuffer* m_pIndexBufferTriangle;
    CConstantBuffer* m_pConstantBufferTriangle;
    float m_fAnimationValue;
};




