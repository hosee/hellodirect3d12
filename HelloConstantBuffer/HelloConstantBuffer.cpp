//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "HelloConstantBuffer.h"
const wchar_t* CHelloConstantBuffer::s_pProjectName = L"HelloConstantBuffer";
const uint32_t CHelloConstantBuffer::s_uiDefaultWindowWidth = 800;
const uint32_t CHelloConstantBuffer::s_uiDefaultWindowHeight = 600;
//
CHelloConstantBuffer::CHelloConstantBuffer( HINSTANCE hInstance ) :
    AApplication( hInstance ),
    m_pDXGIFactory( nullptr ),
    m_pDevice( nullptr ),
    m_pCommandQueue( nullptr ),
    m_pFence( nullptr ),
    m_pCommandAllocator( nullptr ),
    m_pGraphicsCommandList( nullptr ),
    m_pSwapChain( nullptr ),
    m_pRootSignature( nullptr ),
    m_pGraphicsPipelineState( nullptr ),
    m_pRTVHeap( nullptr ),
    m_pVertexBuffer( nullptr ),
    m_pIndexBuffer( nullptr ),
    m_pCSUHeap( nullptr ),
    m_pConstantBuffer( nullptr )
{
    //
    m_hWnd = CreateDefaultWindow();
    if(m_hWnd == nullptr)
      throw std::runtime_error("Create default window was failed.");
    //
#if defined(_DEBUG)
    m_pDXGIFactory = new CDXGIFactory( true );
#else
    m_pDXGIFactory = new CDXGIFactory( false );
#endif
    //
    m_pDevice = new CDevice( m_pDXGIFactory->GetAdapterHandle(0) );
    //
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    m_pCommandQueue = new CCommandQueue( m_pDevice->Handle() , queueDesc);
    //
    m_pFence = new CFence( m_pDevice->Handle() );
    //
    m_pCommandAllocator = new CCommandAllocator( m_pDevice->Handle() , D3D12_COMMAND_LIST_TYPE_DIRECT );
    //
    m_pGraphicsCommandList = new CGraphicsCommandList( m_pDevice->Handle() , m_pCommandAllocator->Handle(), D3D12_COMMAND_LIST_TYPE_DIRECT );
    //
    m_pSwapChain = new CSwapChain( 
      m_pDXGIFactory->Handle(), 
      m_pDevice->Handle(),
      m_pCommandQueue->Handle(), 
      m_hWnd,
      s_uiDefaultWindowWidth, 
      s_uiDefaultWindowHeight, 
      DXGI_FORMAT_R8G8B8A8_UNORM );
    //
    D3D12_DESCRIPTOR_RANGE1 descriptorRangeList[1];
    D3D12_ROOT_PARAMETER1 rootParameterList[1];
    descriptorRangeList[0] = CDirect3DUtil::MakeDescriptorRange( D3D12_DESCRIPTOR_RANGE_TYPE_CBV , 1 , 0 , 0 , D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC );
    rootParameterList[0] = CDirect3DUtil::MakeRootParameter_DescriptorTable( 1 , descriptorRangeList );

    D3D12_ROOT_SIGNATURE_DESC1 rootSignatureDesc={};
    rootSignatureDesc.NumParameters = 1;
    rootSignatureDesc.pParameters = rootParameterList;
    rootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
    m_pRootSignature=new CRootSignature( m_pDevice->Handle() , &rootSignatureDesc , D3D_ROOT_SIGNATURE_VERSION_1_1 );
    //
    ID3DBlob* pVertexShaderBlob = NULL;
    ID3DBlob* pPixelShaderBlob = NULL;
    UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
    CCommonLib::ThrowFailed( 
      D3DCompileFromFile(AssetFullPath(L"Shader/shaders.hlsl").c_str(), nullptr, nullptr, "VSMain", "vs_5_0", compileFlags, 0, &pVertexShaderBlob, nullptr ),
      "Compile vertex shader was failed."
    );
    CCommonLib::ThrowFailed( 
      D3DCompileFromFile(AssetFullPath(L"Shader/shaders.hlsl").c_str(), nullptr, nullptr, "PSMain", "ps_5_0", compileFlags, 0, &pPixelShaderBlob, nullptr ),
      "Compile pixel shader was failed."
    );
    //
    D3D12_INPUT_ELEMENT_DESC inputElementDescs[] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
        { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 } };
    //
    D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
    psoDesc.InputLayout = { inputElementDescs,  sizeof(inputElementDescs) / sizeof(D3D12_INPUT_ELEMENT_DESC) };
    psoDesc.pRootSignature = m_pRootSignature->Handle();
    psoDesc.VS = { pVertexShaderBlob->GetBufferPointer() , pVertexShaderBlob->GetBufferSize() };
    psoDesc.PS = { pPixelShaderBlob->GetBufferPointer() , pPixelShaderBlob->GetBufferSize() };
    psoDesc.RasterizerState=CDirect3DUtil::s_stDefaultRasterizerDesc;
    psoDesc.BlendState = CDirect3DUtil::s_stDefaultBlendDesc;
    psoDesc.DepthStencilState = CDirect3DUtil::s_stDefaultDepthStencilDesc;
    psoDesc.SampleMask = UINT_MAX;
    psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    psoDesc.NumRenderTargets = 1;
    psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
    psoDesc.SampleDesc.Count = 1;
    //
    m_pGraphicsPipelineState = new CGraphicsPipelineState( m_pDevice->Handle() , &psoDesc );
    //
    pVertexShaderBlob->Release();
    pPixelShaderBlob->Release();
    //
    m_pRTVHeap = new CDescriptorHeap( m_pDevice->Handle() , D3D12_DESCRIPTOR_HEAP_TYPE_RTV ,1 );
    //
    const Vertex verticesData[] = {
      { {  0.0f,  0.5f ,  0.0f ,  1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
      { {  0.5f, -0.5f ,  0.0f ,  1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f } },
      { { -0.5f, -0.5f ,  0.0f ,  1.0f }, { 0.0f, 0.0f, 1.0f, 1.0f } } };
    //
    m_pVertexBuffer = new CVertexBuffer( m_pDevice->Handle() , sizeof( Vertex ) , sizeof( verticesData ) , false );
    m_pVertexBuffer->WriteData( ( const uint8_t* )verticesData , sizeof(verticesData) );
    //
    const uint32_t indexData[] = { 0 , 1 , 2 };
    m_pIndexBuffer = new CIndexBuffer( m_pDevice->Handle() , sizeof( indexData ) / sizeof( uint32_t ), DXGI_FORMAT_R32_UINT );
    m_pIndexBuffer->WriteData( ( const uint8_t* )indexData , sizeof( indexData ) );
    //
    m_pCSUHeap = new CDescriptorHeap( m_pDevice->Handle() , D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV , 1 , D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE );
    //
    m_pConstantBuffer = new CConstantBuffer( m_pDevice->Handle() , sizeof( ConstantBuffer ) );
    ConstantBuffer constantBufData={};
    constantBufData.offset.x = 0.5f;
    constantBufData.offset.y = 0.0f;
    constantBufData.offset.z = 0.0f;
    constantBufData.offset.w = 0.0f;
    m_pConstantBuffer->WriteData( ( const uint8_t* )&constantBufData,sizeof( ConstantBuffer ) );
    //
}
CHelloConstantBuffer::~CHelloConstantBuffer()
{
    CCommonLib::SafeReleaseObject( &m_pConstantBuffer );
    CCommonLib::SafeReleaseObject( &m_pCSUHeap );
    CCommonLib::SafeReleaseObject( &m_pIndexBuffer );
    CCommonLib::SafeReleaseObject( &m_pVertexBuffer );
    CCommonLib::SafeReleaseObject( &m_pRTVHeap );
    CCommonLib::SafeReleaseObject( &m_pGraphicsPipelineState );
    CCommonLib::SafeReleaseObject( &m_pRootSignature );
    CCommonLib::SafeReleaseObject( &m_pSwapChain );
    CCommonLib::SafeReleaseObject( &m_pGraphicsCommandList );
    CCommonLib::SafeReleaseObject( &m_pCommandAllocator );
    CCommonLib::SafeReleaseObject( &m_pFence );
    CCommonLib::SafeReleaseObject( &m_pCommandQueue );
    CCommonLib::SafeReleaseObject( &m_pDevice );
    CCommonLib::SafeReleaseObject( &m_pDXGIFactory );
}
HWND CHelloConstantBuffer::CreateDefaultWindow()
{
    RECT rc = { 0, 0, s_uiDefaultWindowWidth, s_uiDefaultWindowHeight };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    HWND hWnd = CreateWindowW( AApplication::s_pMainWindowClassName, s_pProjectName,
                           WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, m_hInstance,
                           this );
    if( !hWnd )
        return nullptr;

    ShowWindow( hWnd , 1 );

    return hWnd;
}
void CHelloConstantBuffer::OnInit()
{
    //Update window title
    std::wstring statusStr = s_pProjectName;
    statusStr += L"  GPU : ";
    statusStr += CDirect3DUtil::GetAdapterInfo( m_pDXGIFactory->GetAdapterHandle(0) );
    statusStr += L" ";
    SetWindowTextW( m_hWnd , statusStr.c_str() );
}
void CHelloConstantBuffer::OnUpdate()
{
    //
    //Record commmand
    //
    m_pCommandAllocator->Reset();
    m_pGraphicsCommandList->Reset( m_pCommandAllocator->Handle() );
    //
    //Process stage buffer
    //
    if( m_pVertexBuffer->NeedSubmitStagingBuffer() )
        m_pVertexBuffer->SubmitStagingBuffer( m_pGraphicsCommandList );
    //
    if( m_pIndexBuffer->NeedSubmitStagingBuffer() )
        m_pIndexBuffer->SubmitStagingBuffer( m_pGraphicsCommandList );
    //
    m_pGraphicsCommandList->BarrierTransition( m_pSwapChain->Buffer(m_pSwapChain->BackBufferIndex() ), D3D12_RESOURCE_STATE_PRESENT , D3D12_RESOURCE_STATE_RENDER_TARGET );
    glm::vec4 clearColor( 0.0f, 0.2f, 0.4f, 1.0f );
    D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle;
    m_pSwapChain->GetBackBufferDescriptorHandle( m_pSwapChain->BackBufferIndex() , rtvHandle );
    m_pGraphicsCommandList->ClearRenderTargetView( rtvHandle, &clearColor[0] );
    //
    D3D12_VIEWPORT vp = {};
    vp.Width = s_uiDefaultWindowWidth;
    vp.Height = s_uiDefaultWindowHeight;
    vp.MaxDepth = 1.0f;
    m_pGraphicsCommandList->RSSetViewports( 1, &vp );
    D3D12_RECT rc = {};
    rc.right = s_uiDefaultWindowWidth;
    rc.bottom = s_uiDefaultWindowHeight;
    m_pGraphicsCommandList->RSSetScissorRects( 1, &rc );
    rtvHandle = m_pRTVHeap->GetCPUDescriptorHandleForHeapStart();
    D3D12_RENDER_TARGET_VIEW_DESC rtvView = {};
    rtvView.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    rtvView.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
    rtvView.Texture2D.MipSlice = 0;
    rtvView.Texture2D.PlaneSlice = 0;
    m_pRTVHeap->WriteRenderTargetView( 0 , m_pSwapChain->Buffer(m_pSwapChain->BackBufferIndex() ), &rtvView );
    m_pGraphicsCommandList->OMSetRenderTargets( 1 , &rtvHandle , TRUE , nullptr );
    m_pGraphicsCommandList->SetDescriptorHeaps(m_pCSUHeap->Handle() );
    m_pGraphicsCommandList->IASetVertexBuffers( 0 , 1, m_pVertexBuffer->VBV() );
    m_pGraphicsCommandList->SetGraphicsRootSignature( m_pRootSignature->Handle() );
    m_pGraphicsCommandList->SetPipelineState( m_pGraphicsPipelineState->Handle() );
    m_pGraphicsCommandList->IASetPrimitiveTopology( D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
    m_pGraphicsCommandList->IASetIndexBuffer( m_pIndexBuffer->IBV() );
    m_pCSUHeap->WriteConstantBufferView( 0 , m_pConstantBuffer->CBV() );
    m_pGraphicsCommandList->SetGraphicsRootDescriptorTable( 0 , m_pCSUHeap->GetGPUDescriptorHandle(0) );
    m_pGraphicsCommandList->DrawIndexedInstanced( 3 , 1 , 0 , 0 , 0 );
    //
    m_pGraphicsCommandList->BarrierTransition( m_pSwapChain->Buffer(m_pSwapChain->BackBufferIndex() ), D3D12_RESOURCE_STATE_RENDER_TARGET , D3D12_RESOURCE_STATE_PRESENT );
    //
    m_pGraphicsCommandList->Close();
    //
    //Submit commands
    //
    m_pCommandQueue->AddCommandList( m_pGraphicsCommandList->Handle() );
    size_t waitCon = m_pCommandQueue->SubmitCommand();
    //
    //Present
    //
    m_pSwapChain->Present();
    //
    //Wait darw end
    //
    m_pFence->WaitFenceCompleted( m_pCommandQueue->Handle() , waitCon );
    m_pSwapChain->UpdateBackBufferIndex();
}