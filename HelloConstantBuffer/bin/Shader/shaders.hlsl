//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

struct VSInput
{
    float4 position : POSITION;
    float4 color : COLOR;
};
cbuffer ConstantBuffer : register(b0)
{
    float4 offset;
};
struct PSInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};
PSInput VSMain( VSInput input )
{
    PSInput result;

    result.position = input.position + offset;
    result.color = input.color;

    return result;
}

float4 PSMain( PSInput input ) : SV_TARGET
{
    return input.color;
}
