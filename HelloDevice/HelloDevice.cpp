//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "HelloDevice.h"
const wchar_t* CHelloDevice::s_pProjectName = L"HelloDevice";
CHelloDevice::CHelloDevice( HINSTANCE hInstance ) :
    AApplication( hInstance ),
    m_pDXGIFactory( nullptr ),
    m_pDevice( nullptr )
{
    //
    m_hWnd = CreateDefaultWindow();
    if(m_hWnd == nullptr)
      throw std::runtime_error("Create default window was failed.");
#if defined(_DEBUG)
    m_pDXGIFactory = new CDXGIFactory( true );
#else
    m_pDXGIFactory = new CDXGIFactory( false );
#endif
    //
    m_pDevice = new CDevice( m_pDXGIFactory->GetAdapterHandle(0) );
}
CHelloDevice::~CHelloDevice()
{
    CCommonLib::SafeReleaseObject( &m_pDevice );
    CCommonLib::SafeReleaseObject( &m_pDXGIFactory );
}
HWND CHelloDevice::CreateDefaultWindow()
{
    RECT rc = { 0, 0, 800, 600 };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    HWND hWnd = CreateWindowW( AApplication::s_pMainWindowClassName, s_pProjectName,
                           WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, m_hInstance,
                           this );
    if( !hWnd )
        return nullptr;

    ShowWindow( hWnd , 1 );

    return hWnd;
}
void CHelloDevice::OnInit()
{
    //Update window title
    std::wstring statusStr = s_pProjectName;
    statusStr += L"  GPU : ";
    statusStr += CDirect3DUtil::GetAdapterInfo( m_pDXGIFactory->GetAdapterHandle(0) );
    statusStr += L" ";
    SetWindowTextW( m_hWnd , statusStr.c_str() );
}