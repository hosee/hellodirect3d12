//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "Application.h"
#include "Direct3DUtil.h"
#include "DXGIFactory.h"
#include "Device.h"
class CHelloDevice : public AApplication
{
public:
    CHelloDevice( HINSTANCE hInstance );
    virtual ~CHelloDevice();
private:
    static const wchar_t* s_pProjectName;
protected:
    HWND CreateDefaultWindow();
    virtual void OnInit();
private:
    CDXGIFactory* m_pDXGIFactory;
    CDevice* m_pDevice;
};