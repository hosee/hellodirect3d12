//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

struct GSInput
{
    float4 position : SV_POSITION;
};
struct PSInput
{
    float4 position : SV_POSITION;
};
static const float3 pos[3] = {
  { 0.0, 0.5, 0.0 },
  { 0.5,-0.5, 0.0 },
  {-0.5,-0.5, 0.0 }
};
PSInput VSMain(uint id: SV_VertexID)
{
    GSInput result;

    result.position = float4( pos[id] , 1.0 );

    return result;
}
[maxvertexcount(12)]
void GSMain(triangle GSInput gin[3],inout TriangleStream<PSInput> gout)
{
  PSInput pin[12];
  //
  pin[0].position=float4( (gin[0].position.xy*0.5)+float2(-0.5,0.5),0.0,1.0);
  pin[1].position=float4( (gin[1].position.xy*0.5)+float2(-0.5,0.5),0.0,1.0);
  pin[2].position=float4( (gin[2].position.xy*0.5)+float2(-0.5,0.5),0.0,1.0);
  gout.Append(pin[0]);
  gout.Append(pin[1]);
  gout.Append(pin[2]);
  gout.RestartStrip();
  //
  pin[3].position=float4( (gin[0].position.xy*0.5)+float2(0.5,0.5),0.0,1.0);
  pin[4].position=float4( (gin[1].position.xy*0.5)+float2(0.5,0.5),0.0,1.0);
  pin[5].position=float4( (gin[2].position.xy*0.5)+float2(0.5,0.5),0.0,1.0);
  gout.Append(pin[3]);
  gout.Append(pin[4]);
  gout.Append(pin[5]);
  gout.RestartStrip();
  //
  pin[6].position=float4( (gin[0].position.xy*0.5)+float2(-0.5,-0.5),0.0,1.0);
  pin[7].position=float4( (gin[1].position.xy*0.5)+float2(-0.5,-0.5),0.0,1.0);
  pin[8].position=float4( (gin[2].position.xy*0.5)+float2(-0.5,-0.5),0.0,1.0);
  gout.Append(pin[6]);
  gout.Append(pin[7]);
  gout.Append(pin[8]);
  gout.RestartStrip();
  //
  pin[9].position=float4( (gin[0].position.xy*0.5)+float2(0.5,-0.5),0.0,1.0);
  pin[10].position=float4( (gin[1].position.xy*0.5)+float2(0.5,-0.5),0.0,1.0);
  pin[11].position=float4( (gin[2].position.xy*0.5)+float2(0.5,-0.5),0.0,1.0);
  gout.Append(pin[9]);
  gout.Append(pin[10]);
  gout.Append(pin[11]);
  gout.RestartStrip();
}
float4 PSMain(PSInput input) : SV_TARGET
{
    return float4( 1.0 , 0.0 , 0.0 , 1.0 );
}
