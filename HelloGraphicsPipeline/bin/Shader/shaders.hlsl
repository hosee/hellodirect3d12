//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

struct PSInput
{
    float4 position : SV_POSITION;
};
static const float3 pos[3] = {
  { 0.0, 0.5, 0.0 },
  { 0.5,-0.5, 0.0 },
  {-0.5,-0.5, 0.0 }
};
PSInput VSMain(uint id: SV_VertexID)
{
    PSInput result;

    result.position = float4( pos[id] , 1.0 );

    return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
    return float4( 1.0 , 0.0 , 0.0 , 1.0 );
}
