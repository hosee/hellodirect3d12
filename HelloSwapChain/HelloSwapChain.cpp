//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "HelloSwapChain.h"
const wchar_t* CHelloSwapChain::s_pProjectName = L"HelloSwapChain";
const uint32_t CHelloSwapChain::s_uiDefaultWindowWidth = 800;
const uint32_t CHelloSwapChain::s_uiDefaultWindowHeight = 600;
//
CHelloSwapChain::CHelloSwapChain( HINSTANCE hInstance ) :
    AApplication( hInstance ),
    m_pDXGIFactory( nullptr ),
    m_pDevice( nullptr ),
    m_pCommandQueue( nullptr ),
    m_pFence( nullptr ),
    m_pCommandAllocator( nullptr ),
    m_pGraphicsCommandList( nullptr ),
    m_pSwapChain( nullptr )
{
    //
    m_hWnd = CreateDefaultWindow();
    if(m_hWnd == nullptr)
      throw std::runtime_error("Create default window was failed.");
#if defined(_DEBUG)
    m_pDXGIFactory = new CDXGIFactory( true );
#else
    m_pDXGIFactory = new CDXGIFactory( false );
#endif
    //
    m_pDevice = new CDevice( m_pDXGIFactory->GetAdapterHandle(0) );
    //
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    m_pCommandQueue = new CCommandQueue( m_pDevice->Handle() , queueDesc);
    //
    m_pFence = new CFence( m_pDevice->Handle() );
    //
    m_pCommandAllocator = new CCommandAllocator( m_pDevice->Handle() , D3D12_COMMAND_LIST_TYPE_DIRECT );
    //
    m_pGraphicsCommandList = new CGraphicsCommandList( m_pDevice->Handle() , m_pCommandAllocator->Handle(), D3D12_COMMAND_LIST_TYPE_DIRECT );
    //
    m_pSwapChain = new CSwapChain( 
      m_pDXGIFactory->Handle(), 
      m_pDevice->Handle(),
      m_pCommandQueue->Handle(), 
      m_hWnd,
      s_uiDefaultWindowWidth, 
      s_uiDefaultWindowHeight, 
      DXGI_FORMAT_R8G8B8A8_UNORM );
    //
}
CHelloSwapChain::~CHelloSwapChain()
{
    CCommonLib::SafeReleaseObject( &m_pSwapChain );
    CCommonLib::SafeReleaseObject( &m_pGraphicsCommandList );
    CCommonLib::SafeReleaseObject( &m_pCommandAllocator );
    CCommonLib::SafeReleaseObject( &m_pFence );
    CCommonLib::SafeReleaseObject( &m_pCommandQueue );
    CCommonLib::SafeReleaseObject( &m_pDevice );
    CCommonLib::SafeReleaseObject( &m_pDXGIFactory );
}
HWND CHelloSwapChain::CreateDefaultWindow()
{
    RECT rc = { 0, 0, s_uiDefaultWindowWidth, s_uiDefaultWindowHeight };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    HWND hWnd = CreateWindowW( AApplication::s_pMainWindowClassName, s_pProjectName ,
                           WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, m_hInstance,
                           this );
    if( !hWnd )
        return nullptr;

    ShowWindow( hWnd , 1 );

    return hWnd;
}
void CHelloSwapChain::OnInit()
{
    //Update window title
    std::wstring statusStr = s_pProjectName;
    statusStr += L"  GPU : ";
    statusStr += CDirect3DUtil::GetAdapterInfo( m_pDXGIFactory->GetAdapterHandle(0) );
    statusStr += L" ";
    SetWindowTextW( m_hWnd , statusStr.c_str() );
}
void CHelloSwapChain::OnUpdate()
{
    //
    //Record commmand
    //
    m_pCommandAllocator->Reset();
    m_pGraphicsCommandList->Reset( m_pCommandAllocator->Handle() );
    //
    m_pGraphicsCommandList->BarrierTransition(m_pSwapChain->Buffer(m_pSwapChain->BackBufferIndex() ), D3D12_RESOURCE_STATE_PRESENT , D3D12_RESOURCE_STATE_RENDER_TARGET );
    glm::vec4 clearColor( 0.0f, 0.2f, 0.4f, 1.0f );
    D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle;
    m_pSwapChain->GetBackBufferDescriptorHandle( m_pSwapChain->BackBufferIndex() , rtvHandle );
    m_pGraphicsCommandList->ClearRenderTargetView( rtvHandle, &clearColor[0] );

    m_pGraphicsCommandList->BarrierTransition(m_pSwapChain->Buffer(m_pSwapChain->BackBufferIndex() ), D3D12_RESOURCE_STATE_RENDER_TARGET , D3D12_RESOURCE_STATE_PRESENT );
    //
    m_pGraphicsCommandList->Close();
    //
    //Submit commands
    //
    m_pCommandQueue->AddCommandList( m_pGraphicsCommandList->Handle() );
    size_t waitCon = m_pCommandQueue->SubmitCommand();
    //
    //Present
    //
    m_pSwapChain->Present();
    //
    //Wait darw end
    //
    m_pFence->WaitFenceCompleted( m_pCommandQueue->Handle() , waitCon );
    m_pSwapChain->UpdateBackBufferIndex();
}