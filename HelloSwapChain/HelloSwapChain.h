//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "glm/vec4.hpp"
#include "Application.h"
#include "Direct3DUtil.h"
#include "DXGIFactory.h"
#include "Device.h"
#include "CommandQueue.h"
#include "Fence.h"
#include "CommandAllocator.h"
#include "GraphicsCommandList.h"
#include "SwapChain.h"
class CHelloSwapChain : public AApplication
{
public:
    CHelloSwapChain( HINSTANCE hInstance );
    virtual ~CHelloSwapChain();
protected:
    HWND CreateDefaultWindow();
    virtual void OnInit();
    virtual void OnUpdate();
private:
    static const wchar_t* s_pProjectName;
    static const uint32_t s_uiDefaultWindowWidth;
    static const uint32_t s_uiDefaultWindowHeight;
private:
    CDXGIFactory* m_pDXGIFactory;
    CDevice* m_pDevice;
    CCommandQueue* m_pCommandQueue;
    CFence* m_pFence;
    CCommandAllocator* m_pCommandAllocator;
    CGraphicsCommandList* m_pGraphicsCommandList;
    CSwapChain* m_pSwapChain;
};