//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

struct VSInput
{
    float4 position : POSITION;
    float4 color : COLOR;
    float2 uv : TEXCOORD;
};
struct PSInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 uv : TEXCOORD;
};
Texture2D texture0 : register(t0);
SamplerState sampler0 : register(s0);

PSInput VSMain( VSInput input )
{
    PSInput result;

    result.position = input.position;
    result.color = input.color;
    result.uv = input.uv;

    return result;
}

float4 PSMain( PSInput input ) : SV_TARGET
{
    return texture0.Sample(sampler0, input.uv);
}
