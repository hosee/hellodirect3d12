//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "CommonLib.h"
#include "Application.h"
#include "HelloTriangle.h"
int WINAPI wWinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow )
{
    try
    {
        CHelloTriangle app( hInstance );
        app.Run();
    }
    catch( std::runtime_error exp )
    {
        std::string str = exp.what();
        str += "\n";
        ::OutputDebugStringA( str.c_str() );
    }
    
    return 0;
}