//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "glm/vec4.hpp"
#include "Application.h"
#include "Direct3DUtil.h"
#include "DXGIFactory.h"
#include "Device.h"
#include "CommandQueue.h"
#include "Fence.h"
#include "CommandAllocator.h"
#include "GraphicsCommandList.h"
#include "SwapChain.h"
#include "RootSignature.h"
#include "GraphicsPipelineState.h"
#include "DescriptorHeap.h"
#include "CommittedResource.h"
#include "VertexBuffer.h"
class CHelloVertexBuffer : public AApplication
{
public:
    CHelloVertexBuffer( HINSTANCE hInstance );
    virtual ~CHelloVertexBuffer();
protected:
    HWND CreateDefaultWindow();
    virtual void OnInit() override;
    virtual void OnUpdate() override;
private:
    static const wchar_t* s_pProjectName;
    static const uint32_t s_uiDefaultWindowWidth;
    static const uint32_t s_uiDefaultWindowHeight;
private:
    struct Vertex
    {
        glm::vec4 position;
        glm::vec4 color;
    };
private:
    CDXGIFactory* m_pDXGIFactory;
    CDevice* m_pDevice;
    CCommandQueue* m_pCommandQueue;
    CFence* m_pFence;
    CCommandAllocator* m_pCommandAllocator;
    CGraphicsCommandList* m_pGraphicsCommandList;
    CSwapChain* m_pSwapChain;
    CRootSignature* m_pRootSignature;
    CGraphicsPipelineState* m_pGraphicsPipelineState;
    CDescriptorHeap* m_pRTVHeap;
    CVertexBuffer* m_pVertexBuffer;
};