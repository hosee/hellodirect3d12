//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#pragma once
#include "Application.h"
class CHelloWindow : public AApplication
{
public:
    CHelloWindow( HINSTANCE hInstance );
    virtual ~CHelloWindow();
protected:
    HWND CreateDefaultWindow();
};